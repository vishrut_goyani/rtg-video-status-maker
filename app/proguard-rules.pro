# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform
-dontwarn org.conscrypt.ConscryptHostnameVerifier

-keepattributes Signature
-keepattributes Annotation
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**

# keep everything in this package from being removed or renamed
-keep class royal.video.status.videostatus.statusmaker.video_maker.modul.model.** { *; }

# keep everything in this package from being renamed only
-keepnames class royal.video.status.videostatus.statusmaker.video_maker.modul.model.** { *; }

# keep everything in this package from being removed or renamed
-keep class royal.video.status.videostatus.statusmaker.video_maker.modul.mergevideodata.models.** { *; }

# keep everything in this package from being renamed only
-keepnames class royal.video.status.videostatus.statusmaker.video_maker.modul.mergevideodata.models.** { *; }


# https://stackoverflow.com/questions/56142150/fatal-exception-java-lang-nullpointerexception-in-release-build
    -keepclassmembers,allowobfuscation class * {
      @com.google.gson.annotations.SerializedName <fields>;
    }

    -keep class com.google.ads.mediation.admob.AdMobAdapter {
        *;
    }

    -keep class com.google.ads.mediation.AdUrlAdapter {
        *;
    }

    -keep class com.google.ads.**
    -dontwarn com.google.ads.**

    -keep public class com.google.firebase.analytics.FirebaseAnalytics {
        public *;
    }

    -keep public class com.google.firebase.crashlytics.FirebaseCrashlytics {
            public *;
        }

    -keep public class com.google.android.gms.measurement.AppMeasurement {
        public *;
    }

    -keepclassmembers class fqcn.of.javascript.interface.for.webview {
       public *;
    }

 # For Google Play Services
     -keep public class com.google.android.gms.ads.**{
        public *;
     }

     # For old ads classes
     -keep public class com.google.ads.**{
        public *;
     }

     # For mediation
     -keepattributes *Annotation*

     # Other required classes for Google Play Services
     # Read more at http://developer.android.com/google/play-services/setup.html
     -keep class * extends java.util.ListResourceBundle {
        protected Object[][] getContents();
     }

     -keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
        public static final *** NULL;
     }

     -keepnames @com.google.android.gms.common.annotation.KeepName class *
     -keepclassmembernames class * {
        @com.google.android.gms.common.annotation.KeepName *;
     }

     -keepnames class * implements android.os.Parcelable {
        public static final ** CREATOR;
     }
