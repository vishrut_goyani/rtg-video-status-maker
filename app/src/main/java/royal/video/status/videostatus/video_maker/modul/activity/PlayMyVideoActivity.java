package royal.video.status.videostatus.video_maker.modul.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;

import java.io.File;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.Global;

public class PlayMyVideoActivity extends AppCompatActivity {
    public static PlayMyVideoActivity playVideoActivity;
    private TextView btnTryAgain;
    public PlayerView exoPlayerVideoDetail;
    private ImageView ibDelete;
    private ImageView ibShare;
    public LinearLayout layoutTryAgain;
    public ProgressBar progressBarExoplayer;
    SimpleExoPlayer simpleExoPlayer;
    String videoFilePath;
    Toolbar toolbar;
    private com.google.android.gms.ads.AdView adm_banner;
    RelativeLayout banner_container;

    private UnifiedNativeAd mNativeAd;
    FrameLayout frameLayout;
    AlertDialog.Builder builder;
    View view;

    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_play_my_video);
        getWindow().setFlags(1024, 1024);

        init();
        //loadInterstitialAdAdmob();
        loadBannerAdmob();
        loadNativeAdsAdmob();

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        playVideoActivity = this;
        Bundle bundle2 = new Bundle();
        bundle2.putString("screen_name", "Play My Video Screen");
        bundle2.putString("full_text", "Play user's creation screen opened");
        if (getIntent().getExtras() != null) {
            videoFilePath = getIntent().getStringExtra("videoFilePath");
        }

        btnTryAgain.setOnClickListener(view -> {
            layoutTryAgain.setVisibility(View.GONE);
            progressBarExoplayer.setVisibility(View.VISIBLE);
            initializePlayer();
        });
        ibDelete.setOnClickListener(view -> {
            pausePlayer();
            deleteVideoDialog();
        });
        ibShare.setOnClickListener(view -> {
            try {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("video/*");
                intent.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(PlayMyVideoActivity.this,
                        Global.strprovider, new File(videoFilePath).getAbsoluteFile()));
                intent.putExtra("android.intent.extra.TEXT", "Royal Video Status Maker..\n\nhttps://play.google.com/store/apps/details?id=" + getPackageName());
                intent.addFlags(1);
                startActivity(Intent.createChooser(intent, "Share Your Image!"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        btnTryAgain = findViewById(R.id.btn_try_again);
        exoPlayerVideoDetail = findViewById(R.id.exo_player_video_detail);
        ibShare = findViewById(R.id.ib_share);
        ibDelete = findViewById(R.id.ib_delete);
        layoutTryAgain = findViewById(R.id.layout_try_again);
        progressBarExoplayer = findViewById(R.id.progressBar_exoplayer);

        view = LayoutInflater.from(this).inflate(R.layout.save_dialog, null);
        frameLayout = view.findViewById(R.id.adFrame);

        banner_container = findViewById(R.id.banner_container);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(getString(R.string.adm_banner_ad));
    }

    public void deleteVideoDialog() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_dialog, null);
        TextView ok = view.findViewById(R.id.btn_okay);
        ImageView cancel = view.findViewById(R.id.btn_cancel);
        ((TextView) view.findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.do_you_want_to_delete_this_video));

        if (mNativeAd != null && frameLayout != null) {
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }
        builder = new AlertDialog.Builder(this, R.style.Widget_Story_DialogTheme)
                .setView(view)
                .setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ok.setOnClickListener(arg0 -> {
            deleteFile(new File(videoFilePath));
            finish();
        });
        cancel.setOnClickListener(v -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
        });
        alertDialog.setOnDismissListener(dialog1 -> {
            if (frameLayout != null) {
                loadNativeAdsAdmob();
            }
            if ((view.findViewById(R.id.container_main)).getParent() != null) {
                ((ViewGroup) view.findViewById(R.id.container_main).getParent()).removeView(view.findViewById(R.id.container_main));
            }
        });
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_dialog, null);
        new AdLoader.Builder(this, getResources().getString(R.string.adm_native_ad)).forUnifiedNativeAd(unifiedNativeAd -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    public void initializePlayer() {
        SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance((Context) this, (TrackSelector) new DefaultTrackSelector((TrackSelection.Factory) new AdaptiveTrackSelection.Factory()));
        simpleExoPlayer = newSimpleInstance;
        exoPlayerVideoDetail.setPlayer(newSimpleInstance);
        simpleExoPlayer.prepare(new ExtractorMediaSource(Uri.parse(videoFilePath), new DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyVideoMakerApplication")), new DefaultExtractorsFactory(), (Handler) null, (ExtractorMediaSource.EventListener) null));
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        exoPlayerVideoDetail.hideController();
        simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
            public void onLoadingChanged(boolean z) {
            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            public void onPositionDiscontinuity(int i) {
            }

            public void onRepeatModeChanged(int i) {
            }

            public void onSeekProcessed() {
            }

            public void onShuffleModeEnabledChanged(boolean z) {
            }

            public void onTimelineChanged(Timeline timeline, Object obj, int i) {
            }

            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }

            public void onPlayerError(ExoPlaybackException exoPlaybackException) {
                if (exoPlaybackException != null && exoPlaybackException.getMessage() != null && exoPlaybackException.getMessage().contains("Unable to connect")) {
                    exoPlayerVideoDetail.hideController();
                    layoutTryAgain.setVisibility(View.VISIBLE);
                }
            }

            public void onPlayerStateChanged(boolean z, int i) {
                int i2;
                ProgressBar progressBar;
                if (i == 2) {
                    progressBar = progressBarExoplayer;
                    i2 = 0;
                } else {
                    progressBar = progressBarExoplayer;
                    i2 = 4;
                }
                progressBar.setVisibility(i2);
            }
        });
    }

    public void pausePlayer() {
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.getPlaybackState();
    }

    private void startPlayer() {
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.getPlaybackState();
    }

    @Override
    public void onBackPressed() {
        //showInterstitialAdAdmob();
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        startPlayer();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (videoFilePath == null && getIntent().getExtras() != null) {
            videoFilePath = getIntent().getStringExtra("videoFilePath");
        }
        initializePlayer();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        simpleExoPlayer.release();
    }

    @Override
    public void onStop() {
        super.onStop();
        simpleExoPlayer.release();
    }

    public void deleteFile(File file) {
        if (file.exists()) {
            if (file.delete()) {
                callBroadCast();
            } else {
            }
        }
        if (!file.exists()) {
            return;
        }
        if (file.delete()) {
            callBroadCast();
            return;
        }
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            MediaScannerConnection.scanFile(this, new String[]{getFilesDir().getAbsolutePath()}, (String[]) null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String str, Uri uri) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("-> uri=");
                    sb.append(uri);
                }
            });
            return;
        }
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
    }

    public void loadInterstitialAdAdmob() {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(
                R.string.adm_interstitial_ad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if (mNativeAd != null)
            mNativeAd.destroy();
        super.onDestroy();
    }
}
