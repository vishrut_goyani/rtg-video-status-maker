package royal.video.status.videostatus.video_maker.modul.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import royal.video.status.videostatus.video_maker.modul.global.Globals;
import royal.video.status.videostatus.video_maker.modul.model.MyCreationVideoData;
import royal.video.status.videostatus.R;

import java.io.File;
import java.util.ArrayList;

public class MyVideoListAdapter extends RecyclerView.Adapter<MyVideoListAdapter.MyViewHolder> {
    private Context context;
    public ArrayList<MyCreationVideoData> videoList;
    VideoSelectListener videoSelectListener;

    public interface VideoSelectListener {
        void onVideoSelectListener(int i, MyCreationVideoData myCreationVideoData);
    }

    public MyVideoListAdapter(ArrayList<MyCreationVideoData> arrayList, Context context2, VideoSelectListener videoSelectListener2) {
        this.context = context2;
        this.videoList = arrayList;
        this.videoSelectListener = videoSelectListener2;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(this.context).inflate(R.layout.row_layout_video_list, viewGroup, false));
    }

    public void onBindViewHolder(final MyViewHolder myViewHolder, int position) {
        MyCreationVideoData myCreationVideoData = this.videoList.get(position);
//        myViewHolder.layoutVideoName.setVisibility(View.GONE);
        myViewHolder.tv_video_name.setText(new File(myCreationVideoData.getFilePath()).getName());
        int density = (Resources.getSystem().getDisplayMetrics().widthPixels / 2) - Globals.getDensity(8.0d);
        myViewHolder.videoListThumb.setLayoutParams(new RelativeLayout.LayoutParams(density, (myCreationVideoData.getHeight() * (density - Globals.getDensity(5.0d))) / myCreationVideoData.getWidth()));
        Glide.with(this.context).load(Uri.fromFile(new File(myCreationVideoData.getFilePath()))).into(myViewHolder.videoListThumb);
        myViewHolder.cvVideoList.setOnClickListener(view -> {
            if (videoSelectListener != null) {
                videoSelectListener.onVideoSelectListener(myViewHolder.getAdapterPosition(), videoList.get(myViewHolder.getAdapterPosition()));
            }
        });
    }

    public int getItemCount() {
        return this.videoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout change_border;
        public CardView cvVideoList;
        ConstraintLayout layoutVideoName;
        public ImageView videoListThumb;
        TextView tv_video_name;

        public MyViewHolder(View view) {
            super(view);
            layoutVideoName = view.findViewById(R.id.layout_video_name);
            videoListThumb = view.findViewById(R.id.video_list_thumb);
            cvVideoList = view.findViewById(R.id.cv_video_list);
            change_border = view.findViewById(R.id.change_border);
            tv_video_name = view.findViewById(R.id.tv_video_name);
        }
    }
}
