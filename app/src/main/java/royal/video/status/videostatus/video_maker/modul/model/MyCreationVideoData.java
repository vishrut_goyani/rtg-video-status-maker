package royal.video.status.videostatus.video_maker.modul.model;

public class MyCreationVideoData {
    private String fileName;
    private String filePath;
    private int height;
    private int width;

    public MyCreationVideoData(String str, String str2, int i, int i2) {
        this.fileName = str;
        this.filePath = str2;
        this.height = i;
        this.width = i2;
    }

    public int getHeight() {
        return this.height;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public int getWidth() {
        return this.width;
    }
}
