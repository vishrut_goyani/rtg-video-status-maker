package royal.video.status.videostatus.video_maker.modul.mergevideodata.models;


import royal.video.status.videostatus.video_maker.modul.mergevideodata.OnEditorListener;

import Jni.FFmpegCmd;

public class EpEditor {
    private static final int DEFAULT_HEIGHT = 360;
    private static final int DEFAULT_WIDTH = 480;

    public enum Format {
        MP3,
        MP4
    }

    public enum PTS {
        VIDEO,
        AUDIO,
        ALL
    }

    public static class OutputOption {
        static final int FOUR_TO_THREE = 2;
        static final int NINE_TO_SIXTEEN = 4;
        static final int ONE_TO_ONE = 1;
        static final int SIXTEEN_TO_NINE = 3;
        static final int THREE_TO_FOUR = 5;
        public int bitRate = 0;
        public int frameRate = 0;
        private int height = 0;
        public String outFormat = "";
        String outPath;
        private int sar = 6;
        private int width = 0;

        public OutputOption(String str) {
            this.outPath = str;
        }

        public String getSar() {
            int i = this.sar;
            if (i == 1) {
                return "1/1";
            }
            if (i == 2) {
                return "4/3";
            }
            if (i == 3) {
                return "16/9";
            }
            if (i == 4) {
                return "9/16";
            }
            if (i == 5) {
                return "3/4";
            }
            return this.width + "/" + this.height;
        }

        public void setSar(int i) {
            this.sar = i;
        }

        public String getOutputInfo() {
            StringBuilder sb = new StringBuilder();
            if (this.frameRate != 0) {
                sb.append(" -r ");
                sb.append(this.frameRate);
            }
            if (this.bitRate != 0) {
                sb.append(" -b ");
                sb.append(this.bitRate);
                sb.append("M");
            }
            if (!this.outFormat.isEmpty()) {
                sb.append(" -f ");
                sb.append(this.outFormat);
            }
            return sb.toString();
        }

        public void setWidth(int i) {
            if (i % 2 != 0) {
                i--;
            }
            this.width = i;
        }

        public void setHeight(int i) {
            if (i % 2 != 0) {
                i--;
            }
            this.height = i;
        }
    }

    private EpEditor() {
    }



    public static void execCmd(CmdList cmdList, long j, final OnEditorListener onEditorListener) {
        FFmpegCmd.exec((String[]) cmdList.toArray(new String[cmdList.size()]), j, new VideoHandle.OnEditorListener() {
            @Override
            public void onSuccess() {
                onEditorListener.onSuccess();
            }

            @Override
            public void onFailure() {
                onEditorListener.onFailure();
            }

            @Override
            public void onProgress(float progress) {
                onEditorListener.onProgress(progress);
            }


        });
    }
}
