package royal.video.status.videostatus.video_maker.modul.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import royal.video.status.videostatus.R;
import royal.video.status.videostatus.utils.PreferenceUtility;
import royal.video.status.videostatus.video_maker.modul.activity.PlayVideoActivity;
import royal.video.status.videostatus.video_maker.modul.adapter.VideoListAdapter;
import royal.video.status.videostatus.video_maker.modul.api.APIClient;
import royal.video.status.videostatus.video_maker.modul.api.APIInterface;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoList;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoListDataByCategory;

public class DynamicFragment extends Fragment implements VideoListAdapter.VideoSelectListener {
    RecyclerView catData;
    int catID;
    String catName;
    Context context;
    public ModelVideoListDataByCategory modelVideoList;
    ProgressBar progressCat;
    TextView textNoData;
    public VideoListAdapter videoListAdapter;
    public ArrayList<ModelVideoList> videoLists = new ArrayList<>();
    View view;
    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;

    @Override
    public void onResume() {
        super.onResume();
        //loadInterstitialAdAdmob();
    }

    public static Fragment newInstance(int i, String str) {
        DynamicFragment dynamicFragment = new DynamicFragment();
        Bundle bundle = new Bundle();
        bundle.putString("catName", str);
        bundle.putInt("catID", i);
        dynamicFragment.setArguments(bundle);
        return dynamicFragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.fragment_list, viewGroup, false);
        catName = getArguments().getString("catName");
        catID = getArguments().getInt("catID");
        context = viewGroup.getContext();
        catData = view.findViewById(R.id.catData);
        textNoData = view.findViewById(R.id.text_no_data);
        progressCat = view.findViewById(R.id.progressCat);
        callApi();
        return view;
    }

    public void callApi() {
        APIClient.getClient().create(APIInterface.class).getVideoListByCategory(catName, String.valueOf(catID)).enqueue(new Callback<ModelVideoListDataByCategory>() {
            public void onResponse(Call<ModelVideoListDataByCategory> call, Response<ModelVideoListDataByCategory> response) {
                modelVideoList = response.body();
                try {
                    if (modelVideoList.getData() != null) {
                        DynamicFragment dynamicFragment = DynamicFragment.this;
                        dynamicFragment.videoLists = dynamicFragment.modelVideoList.getData().getTemplatesList();
                        progressCat.setVisibility(View.GONE);
                        catData.setVisibility(View.VISIBLE);
                        if (videoLists != null) {
                            VideoListAdapter unused = videoListAdapter = new VideoListAdapter(videoLists, context, DynamicFragment.this);
                            catData.setAdapter(videoListAdapter);
                            return;
                        }
                        textNoData.setVisibility(View.VISIBLE);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressCat.setVisibility(View.GONE);
                textNoData.setVisibility(View.VISIBLE);
            }

            public void onFailure(Call<ModelVideoListDataByCategory> call, Throwable th) {
                call.cancel();
            }
        });
    }

    @Override
    public void onVideoSelectListener(int i, ModelVideoList modelVideoList2) {
        Intent intent = new Intent(context, PlayVideoActivity.class);
        intent.putExtra("video_object", new Gson().toJson((Object) modelVideoList2));
        context.startActivity(intent);
    }

    public void loadInterstitialAdAdmob() {
        if (mInterstitialAd != null)
            mInterstitialAd = null;
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(context);
        mInterstitialAd.setAdUnitId(getResources().getString(
                R.string.adm_interstitial_ad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }
}
