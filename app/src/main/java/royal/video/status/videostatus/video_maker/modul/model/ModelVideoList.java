package royal.video.status.videostatus.video_maker.modul.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelVideoList implements Serializable {
    @SerializedName("created")
    private long Created;
    @SerializedName("height")
    private int Height = 50;
    @SerializedName("id")

    private int f186Id;
    @SerializedName("is_hot")
    private boolean IsHot;
    @SerializedName("is_new")
    private boolean IsNew;
    @SerializedName("thumb_url")
    private String ThumbUrl;
    @SerializedName("title")
    private String Title;
    @SerializedName("video_url")
    private String VideoUrl;
    @SerializedName("width")
    private int Width = 100;
    @SerializedName("zip")
    private String Zip = "";
    @SerializedName("zip_url")
    private String ZipUrl;

    public long getCreated() {
        return this.Created;
    }

    public int getHeight() {
        return this.Height;
    }

    public int getId() {
        return this.f186Id;
    }

    public String getThumbUrl() {
        return this.ThumbUrl;
    }

    public String getTitle() {
        return this.Title;
    }

    public String getVideoUrl() {
        return this.VideoUrl;
    }

    public int getWidth() {
        return this.Width;
    }

    public String getZip() {
        return this.Zip;
    }

    public String getZipUrl() {
        return this.ZipUrl;
    }

    public boolean getIsHot() {
        return this.IsHot;
    }

    public boolean getIsNew() {
        return this.IsNew;
    }
}
