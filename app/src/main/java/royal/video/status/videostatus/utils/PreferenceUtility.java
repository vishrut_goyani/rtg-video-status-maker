package royal.video.status.videostatus.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtility {
    public static SharedPreferences prefs;
    private static PreferenceUtility mInstance;
    public static Context mContext;
    public static final String APP_PREFS = "VIDEO_STORY_PREFS";
    public static final String is_rated = "is_rated";
    public static final String count_ = "counts";
    public static String RECEIVER_ADDRESS = "info@rtechnogiants.com";

    public PreferenceUtility(Context context) {
        mContext = context;
        try {
            prefs = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized PreferenceUtility getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceUtility(context);
        }
        return mInstance;
    }

    public static void setRated(boolean rated) {
        prefs.edit().putBoolean(is_rated, rated).apply();
    }

    public static boolean getRated() {
        return prefs.getBoolean(is_rated, false);
    }

    public static void setCount(int count) {
        prefs.edit().putInt(count_, count).apply();
    }

    public static int getCounts() {
        return prefs.getInt(count_, 0);
    }
}
