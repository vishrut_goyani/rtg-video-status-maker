package royal.video.status.videostatus.video_maker.modul.mergevideodata.models;

import java.util.ArrayList;
import java.util.Iterator;

public class CmdList extends ArrayList<String> {
    public CmdList append(String str) {
        add(str);
        return this;
    }

    public CmdList append(int i) {
        add(i + "");
        return this;
    }

    public CmdList append(float f) {
        add(f + "");
        return this;
    }

    public CmdList append(StringBuilder sb) {
        add(sb.toString());
        return this;
    }

    public CmdList append(String[] strArr) {
        for (String str : strArr) {
            if (!str.replace(" ", "").equals("")) {
                add(str);
            }
        }
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(" ");
            sb.append((String) it.next());
        }
        return sb.toString();
    }
}
