package royal.video.status.videostatus.video_maker.modul.mergevideodata;

import android.media.MediaExtractor;
import android.media.MediaFormat;

public class TrackUtils {
    private static final String TAG = "TrackUtils";

    public static int selectVideoTrack(MediaExtractor mediaExtractor) {
        int trackCount = mediaExtractor.getTrackCount();
        for (int i = 0; i < trackCount; i++) {
            MediaFormat trackFormat = mediaExtractor.getTrackFormat(i);
            String string = trackFormat.getString("mime");
            if (string.startsWith("videojodnecreationapp/")) {
                return i;
            }
        }
        return -1;
    }

    public static int selectAudioTrack(MediaExtractor mediaExtractor) {
        int trackCount = mediaExtractor.getTrackCount();
        for (int i = 0; i < trackCount; i++) {
            MediaFormat trackFormat = mediaExtractor.getTrackFormat(i);
            String string = trackFormat.getString("mime");
            if (string.startsWith("audio/")) {
                return i;
            }
        }
        return -1;
    }
}
