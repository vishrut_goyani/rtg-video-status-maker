package royal.video.status.videostatus.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.LinkedHashMap;

public class AppUtil {

    public static void putInAdjustsContrast(Context context, String str, float f) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Data Holder", 0);
        Editor edit = sharedPreferences.edit();
        String str2 = "adjustContracts";
        LinkedHashMap linkedHashMap = (LinkedHashMap) new Gson().fromJson(sharedPreferences.getString(str2, ""), new TypeToken<LinkedHashMap<String, Float>>() {
        }.getType());
        linkedHashMap.put(str, Float.valueOf(f));
        edit.putString(str2, new Gson().toJson((Object) linkedHashMap));
        edit.commit();
    }

}
