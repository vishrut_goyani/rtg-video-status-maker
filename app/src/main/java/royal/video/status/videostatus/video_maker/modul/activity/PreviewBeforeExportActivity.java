package royal.video.status.videostatus.video_maker.modul.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import java.io.File;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoList;

public class PreviewBeforeExportActivity extends AppCompatActivity {

    MaterialButton ic_export;
    private PlayerView exoPlayerVideoDetail;
    String videoName = "";
    ModelVideoList videoObject;
    String videoPath = "";
    SimpleExoPlayer simpleExoPlayer;
    ProgressBar progressBarExoplayer;
    Toolbar toolbar;
    LinearLayout nativeAdView;
    private UnifiedNativeAd mNativeAd;
    FrameLayout frameLayout;
    RelativeLayout banner_container;
    AlertDialog.Builder builder;
    View view;

    com.google.android.gms.ads.InterstitialAd mInterstitialAd;
    private com.google.android.gms.ads.AdView adm_banner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_before_export);
        init();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        loadBannerAdmob();

        if (getIntent().getExtras() != null) {
            videoObject = new Gson().fromJson(getIntent().getStringExtra("video_object"), ModelVideoList.class);
            videoPath = getIntent().getStringExtra("filePath");
            videoName = getIntent().getStringExtra("fileName");
        }
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        ic_export = findViewById(R.id.ic_export);
        progressBarExoplayer = findViewById(R.id.progressBar_exoplayer);
        exoPlayerVideoDetail = findViewById(R.id.exo_player_video_detail);
        banner_container = findViewById(R.id.banner_container);
        view = LayoutInflater.from(this).inflate(R.layout.save_dialog, null);
        frameLayout = view.findViewById(R.id.adFrame);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(getString(R.string.adm_banner_ad));

        ic_export.setOnClickListener(v -> {
            saveDiscardDialog(true);
        });
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    private void showAdmobInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void pausePlayer() {
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.getPlaybackState();
    }

    private void startPlayer() {
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.getPlaybackState();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        simpleExoPlayer.release();
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        startPlayer();
        loadAdmobInterStitial();
        loadNativeAdsAdmob();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (videoObject == null) {
            videoObject = new Gson().fromJson(getIntent().getStringExtra("video_object"),
                    ModelVideoList.class);
            videoPath = getIntent().getStringExtra("filePath");
            videoName = getIntent().getStringExtra("fileName");
        }
        initializePlayer();
    }

    public void loadAdmobInterStitial() {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(
                R.string.adm_interstitial_ad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    private void initializePlayer() {
        SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(new AdaptiveTrackSelection.Factory()));
        simpleExoPlayer = newSimpleInstance;
        exoPlayerVideoDetail.setPlayer(newSimpleInstance);
        simpleExoPlayer.prepare(new ExtractorMediaSource(Uri.parse(videoPath), new DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyVideoMakerApplication")), new DefaultExtractorsFactory(), null, null));
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        exoPlayerVideoDetail.hideController();
        simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
            public void onLoadingChanged(boolean z) {
            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            public void onPlayerError(ExoPlaybackException exoPlaybackException) {
            }

            public void onPositionDiscontinuity(int i) {
            }

            public void onRepeatModeChanged(int i) {
            }

            public void onSeekProcessed() {
            }

            public void onShuffleModeEnabledChanged(boolean z) {
            }

            public void onTimelineChanged(Timeline timeline, Object obj, int i) {
            }

            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }

            public void onPlayerStateChanged(boolean z, int i) {
                int i2;
                ProgressBar progressBar;
                if (i == 2) {
                    progressBar = progressBarExoplayer;
                    i2 = 0;
                } else {
                    progressBar = progressBarExoplayer;
                    i2 = 4;
                }
                progressBar.setVisibility(i2);
            }
        });
    }

    @Override
    public void onBackPressed() {
        saveDiscardDialog(false);
    }

    public void saveDiscardDialog(boolean isSaveDialog) {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_dialog, null);
        TextView ok = view.findViewById(R.id.btn_okay);
        ImageView cancel = view.findViewById(R.id.btn_cancel);

        if (isSaveDialog) {
            ((TextView) view.findViewById(R.id.txtTitle)).setText(R.string.dialog_save_text);
        } else {
            ((TextView) view.findViewById(R.id.txtTitle)).setText(getResources().getString(R.string.dialog_discard_text));
        }

        if (mNativeAd != null && frameLayout != null) {
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }
        builder = new AlertDialog.Builder(this, R.style.Widget_Story_DialogTheme)
                .setView(view)
                .setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ok.setOnClickListener(arg0 -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            if (isSaveDialog) {
                Intent intent = new Intent(this, VideoPreviewActivity.class);
                intent.putExtra("filePath", videoPath);
                intent.putExtra("fileName", videoName);
                intent.putExtra("video_object", new Gson().toJson(videoObject));
                startActivity(intent);
                showAdmobInterstitial();
            } else {
                new DiscardVideo().execute(videoPath);
            }
        });
        cancel.setOnClickListener(v -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
        });
        alertDialog.setOnDismissListener(dialog1 -> {
            if (frameLayout != null) {
                loadNativeAdsAdmob();
            }
            if ((view.findViewById(R.id.container_main)).getParent() != null) {
                ((ViewGroup) view.findViewById(R.id.container_main).getParent()).removeView(view.findViewById(R.id.container_main));
            }
        });
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_dialog, null);
        new AdLoader.Builder(this, getResources().getString(R.string.adm_native_ad)).forUnifiedNativeAd(unifiedNativeAd -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    private class DiscardVideo extends AsyncTask<String, Void, Boolean> {
        ProgressDialog pd = new ProgressDialog(PreviewBeforeExportActivity.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setTitle("Deleting video...");
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Boolean doInBackground(String... filePath) {
            File file = new File(filePath[0]);
            if (file.exists()) {
                if (file.delete()) {
                    callBroadCast();
                } else {
                    return false;
                }
            }
            if (!file.exists()) {
                return true;
            }
            if (file.delete()) {
                callBroadCast();
                return true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean successful) {
            super.onPostExecute(successful);
            pd.dismiss();
            if (successful)
                Toast.makeText(PreviewBeforeExportActivity.this, "Video discarded", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            MediaScannerConnection.scanFile(this, new String[]{getFilesDir().getAbsolutePath()}, (String[]) null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String str, Uri uri) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("-> uri=");
                    sb.append(uri);
                }
            });
            return;
        }
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
    }

    @Override
    protected void onDestroy() {
        if (mNativeAd != null) {
            mNativeAd.destroy();
        }
        super.onDestroy();
    }
}
