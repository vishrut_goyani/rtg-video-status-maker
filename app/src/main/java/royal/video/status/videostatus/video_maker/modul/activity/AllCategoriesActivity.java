package royal.video.status.videostatus.video_maker.modul.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.modul.adapter.DynamicAdapter;
import royal.video.status.videostatus.video_maker.modul.api.APIClient;
import royal.video.status.videostatus.video_maker.modul.api.APIInterface;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoListData;
import royal.video.status.videostatus.video_maker.modul.model.VideoCategoryData;

public class AllCategoriesActivity extends AppCompatActivity {
    RecyclerView rvCategories;
    String category = "popular";
    public ArrayList<VideoCategoryData> categoryList;
    ModelVideoListData modelVideoList;
    ProgressDialog progressDialog;
    DynamicAdapter dynamicAdapter;
    Toolbar toolbar;
//    int[] images = new int[]{
//            R.drawable.cat_latest,
//            R.drawable.cat_fathers_day,
//            R.drawable.cat_love,
//            R.drawable.cat_news,
//            R.drawable.cat_lyrical,
//            R.drawable.cat_particle,
//            R.drawable.cat_magical,
//            R.drawable.cat_dialogue,
//            R.drawable.cat_birthday,
//            R.drawable.cat_wedding,
//            R.drawable.cat_love,
//            R.drawable.cat_navratri,
//            R.drawable.cat_friendship,
//            R.drawable.cat_maa,
//            R.drawable.cat_indian,
//            R.drawable.cat_republic_day,
//            R.drawable.cat_rakhi,
//            R.drawable.cat_janmashtami,
//            R.drawable.cat_aug,
//            R.drawable.cat_ganesha,
//            R.drawable.cat_diwali,
//            R.drawable.cat_christmas,
//            R.drawable.cat_ttto,
//            R.drawable.cat_kite,
//            R.drawable.cat_valentine,
//            R.drawable.cat_holi,
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        rvCategories = findViewById(R.id.rvCategories);
        rvCategories.setLayoutManager(new LinearLayoutManager(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching data...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        callApi(category);
    }

    public void callApi(String str) {
        APIClient.getClient().create(APIInterface.class).getVideoList(str).
                enqueue(new Callback<ModelVideoListData>() {
                    public void onResponse(Call<ModelVideoListData> call, Response<ModelVideoListData> response) {
                        categoryList = new ArrayList<>();
                        modelVideoList = response.body();
                        categoryList = modelVideoList.getData().getCategoriesList();
                        int[] iArr = {1, 10};
                        for (int i = 1; i >= 0; i--) {
                            categoryList.remove(iArr[i]);
                        }

                        ArrayList<VideoCategoryData> categoryListNew = new ArrayList<>(categoryList);

                        dynamicAdapter = new DynamicAdapter(AllCategoriesActivity.this, categoryListNew);
                        rvCategories.setAdapter(dynamicAdapter);
                        progressDialog.dismiss();
                    }

                    public void onFailure(Call<ModelVideoListData> call, Throwable th) {
                        call.cancel();
                        Toast.makeText(AllCategoriesActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
    }
}