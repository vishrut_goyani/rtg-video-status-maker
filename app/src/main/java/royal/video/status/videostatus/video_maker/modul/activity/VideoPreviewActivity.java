package royal.video.status.videostatus.video_maker.modul.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.gson.Gson;

import java.io.File;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.Global;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoList;

public class VideoPreviewActivity extends AppCompatActivity {
    private PlayerView exoPlayerVideoDetail;
    String videoName = "";
    ModelVideoList videoObject;
    String videoPath = "";
    public ProgressBar progressBarExoplayer;
    SimpleExoPlayer simpleExoPlayer;
    Toolbar toolbar;
    AppCompatImageView ic_back_home;

    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;

    private com.google.android.gms.ads.AdView adm_banner;
    RelativeLayout banner_container;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video_preview);
        init();
        loadBannerAdmob();
        loadAdmobInterStitial();

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        ic_back_home.setOnClickListener(v -> {
            Intent intent = new Intent(this, StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //showInterstitialAdAdmob();
            finish();
        });

        Bundle bundle2 = new Bundle();
        bundle2.putString("screen_name", "Video Preview Screen");
        bundle2.putString("full_text", "Preview user's created video and share screen opened");
        if (getIntent().getExtras() != null) {
            videoObject = new Gson().fromJson(getIntent().getStringExtra("video_object"), ModelVideoList.class);
            videoPath = getIntent().getStringExtra("filePath");
            videoName = getIntent().getStringExtra("fileName");
        }
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        ic_back_home = findViewById(R.id.ic_back_home);
        exoPlayerVideoDetail = findViewById(R.id.exo_player_video_detail);
        progressBarExoplayer = findViewById(R.id.progressBar_exoplayer);

        banner_container = findViewById(R.id.banner_container);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(getString(R.string.adm_banner_ad));
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    public void loadAdmobInterStitial() {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(
                R.string.adm_interstitial_ad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("WrongConstant")
    public void handleClick(View view) {
        switch (view.getId()) {
            case R.id.iv_fb:
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("video/*");
                intent.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(this, Global.strprovider, new File(videoPath).getAbsoluteFile()));
                intent.putExtra("android.intent.extra.TITLE", "Royal Video Status Maker..\n\nhttps://play.google.com/store/apps/details?id=" + getPackageName());
                intent.setPackage("com.facebook.katana");
                intent.addFlags(1);
                if (appInstalledOrNot("com.facebook.katana")) {
                    try {
                        startActivity(Intent.createChooser(intent, "Share images..."));
                        return;
                    } catch (ActivityNotFoundException unused) {
                        Toast.makeText(this, "Please Install Facebook", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    Toast.makeText(this, "Please Install Facebook", Toast.LENGTH_LONG).show();
                    return;
                }
            case R.id.iv_insta:
                Intent intent2 = new Intent("android.intent.action.SEND");
                intent2.setType("video/*");
                intent2.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(this, Global.strprovider, new File(videoPath).getAbsoluteFile()));
                intent2.putExtra("android.intent.extra.TITLE", "Royal Video Status Maker..\n\nhttps://play.google.com/store/apps/details?id=" + getPackageName());
                intent2.setPackage("com.instagram.android");
                intent2.addFlags(1);
                if (appInstalledOrNot("com.instagram.android")) {
                    try {
                        startActivity(Intent.createChooser(intent2, "Share images..."));
                        return;
                    } catch (ActivityNotFoundException unused2) {
                        Toast.makeText(this, "Please Install Instagram", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    Toast.makeText(this, "Please Install Instagram", Toast.LENGTH_LONG).show();
                    return;
                }
            case R.id.iv_share:
                try {
                    Intent intent3 = new Intent("android.intent.action.SEND");
                    intent3.setType("video/*");
                    intent3.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(this, Global.strprovider, new File(videoPath).getAbsoluteFile()));
                    intent3.putExtra("android.intent.extra.TEXT", "Royal Video Status Maker..\n\nhttps://play.google.com/store/apps/details?id=" + getPackageName());
                    intent3.addFlags(1);
                    startActivity(Intent.createChooser(intent3, "Share Your Image!"));
                    return;
                } catch (Exception unused3) {
                    return;
                }
            case R.id.iv_wa:
                Intent intent4 = new Intent("android.intent.action.SEND");
                intent4.setType("video/*");
                intent4.putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(this, Global.strprovider, new File(videoPath).getAbsoluteFile()));
                intent4.putExtra("android.intent.extra.TEXT", "Royal Video Status Maker..\n\nhttps://play.google.com/store/apps/details?id=" + getPackageName());
                intent4.setPackage("com.whatsapp");
                intent4.addFlags(1);
                if (appInstalledOrNot("com.whatsapp")) {
                    try {
                        startActivity(Intent.createChooser(intent4, "Share images..."));
                        return;
                    } catch (ActivityNotFoundException unused4) {
                        Toast.makeText(this, "Please Install WhatsApp", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    Toast.makeText(this, "Please Install WhatsApp", Toast.LENGTH_LONG).show();
                    return;
                }
            default:
                return;
        }
    }

    private boolean appInstalledOrNot(String str) {
        try {
            getPackageManager().getPackageInfo(str, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    private void initializePlayer() {
        SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(new AdaptiveTrackSelection.Factory()));
        simpleExoPlayer = newSimpleInstance;
        exoPlayerVideoDetail.setPlayer(newSimpleInstance);
        simpleExoPlayer.prepare(new ExtractorMediaSource(Uri.parse(videoPath), new DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyVideoMakerApplication")), new DefaultExtractorsFactory(), null, null));
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        exoPlayerVideoDetail.hideController();
        simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
            public void onLoadingChanged(boolean z) {
            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            public void onPlayerError(ExoPlaybackException exoPlaybackException) {
            }

            public void onPositionDiscontinuity(int i) {
            }

            public void onRepeatModeChanged(int i) {
            }

            public void onSeekProcessed() {
            }

            public void onShuffleModeEnabledChanged(boolean z) {
            }

            public void onTimelineChanged(Timeline timeline, Object obj, int i) {
            }

            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }

            public void onPlayerStateChanged(boolean z, int i) {
                int i2;
                ProgressBar progressBar;
                if (i == 2) {
                    progressBar = progressBarExoplayer;
                    i2 = 0;
                } else {
                    progressBar = progressBarExoplayer;
                    i2 = 4;
                }
                progressBar.setVisibility(i2);
            }
        });
    }

    public void pausePlayer() {
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.getPlaybackState();
    }

    private void startPlayer() {
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.getPlaybackState();
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        startPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        simpleExoPlayer.release();
    }

    @Override
    public void onStop() {
        super.onStop();
        simpleExoPlayer.release();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (videoObject == null) {
            videoObject = new Gson().fromJson(getIntent().getStringExtra("video_object"),
                    ModelVideoList.class);
            videoPath = getIntent().getStringExtra("filePath");
            videoName = getIntent().getStringExtra("fileName");
        }
        initializePlayer();
    }
}
