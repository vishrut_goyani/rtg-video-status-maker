package royal.video.status.videostatus.video_maker.modul.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;

import dev.yuganshtyagi.smileyrating.SmileyRatingView;
import royal.video.status.videostatus.BuildConfig;
import royal.video.status.videostatus.R;
import royal.video.status.videostatus.interfaces.FeedbackListener;
import royal.video.status.videostatus.utils.PreferenceUtility;
import royal.video.status.videostatus.widgets.FeedbackSheetBuilder;


public class StartActivity extends AppCompatActivity implements View.OnClickListener, FeedbackListener {
    private static final int CHECK_UPDATE_REQUEST_CODE = 151;
    FloatingActionButton fab_create_video;
    RelativeLayout rl_creations, rl_popular;
    Toolbar toolbar;
    DrawerLayout drawerLayout;

    UnifiedNativeAd mUnifiedNativeAd;
    LinearLayout nativeAdView, nativeAdViewDialog;
    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;
    ConstraintLayout nav_rateUs, nav_feedback, nav_shareApp;
    public static FeedbackListener feedbackListenerMain;
    FeedbackSheetBuilder feedbackSheetBuilder;
    SmileyRatingView smiley_view;
    AppCompatRatingBar ratingBar;
    TextView tv_submit, tv_maybe_later, rate_text;
    float rating;
    View view;
    FrameLayout adFrame;
    FrameLayout frameLayout;
    BottomAppBar bottomAppBar;

    BottomSheetDialog mDialog;
    boolean isShowing;
    AppUpdateManager appUpdateManager;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_start);
        init();
        feedbackListenerMain = this;
        view = LayoutInflater.from(this).inflate(R.layout.app_exit_dialog, null);
        adFrame = view.findViewById(R.id.adFrame);
        mDialog = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
        mDialog.requestWindowFeature(1);
        mDialog.setContentView(view);
        checkForAppUpdate();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.START));
        findViewById(R.id.rl_popular).setOnClickListener(v -> {
            startActivity(new Intent(StartActivity.this, VideoCategoryDetailsActivity.class)
                    .putExtra("catId", 14)
                    .putExtra("catName", "Popular"));
        });
        rl_creations.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= 23 && !(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1001);
            } else {
                startActivity(new Intent(StartActivity.this, MyVideosActivity.class));
                showAdmobInterstitial();
            }
        });
        fab_create_video.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= 23 && !(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1002);
            } else {
                startActivity(new Intent(StartActivity.this, AllCategoriesActivity.class));
                showAdmobInterstitial();
            }
        });
    }

    private void init() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        fab_create_video = findViewById(R.id.fab_create_video);
        rl_creations = findViewById(R.id.rl_creations);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawerLayout);
        nav_rateUs = findViewById(R.id.nav_rateUs);
        nav_feedback = findViewById(R.id.nav_feedback);
        nav_shareApp = findViewById(R.id.nav_shareApp);
        frameLayout = findViewById(R.id.frameLayout);
        bottomAppBar = findViewById(R.id.bottom_app_bar);

        MaterialShapeDrawable bottomBarBackground = (MaterialShapeDrawable) bottomAppBar.getBackground();
        bottomBarBackground.setShapeAppearanceModel(
                bottomBarBackground.getShapeAppearanceModel()
                        .toBuilder()
                        .setTopRightCorner(CornerFamily.ROUNDED, 48)
                        .setTopLeftCorner(CornerFamily.ROUNDED, 48)
                        .build());

        nav_rateUs.setOnClickListener(this);
        nav_feedback.setOnClickListener(this);
        nav_shareApp.setOnClickListener(this);
    }

    private void checkForAppUpdate() {
        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(result -> {
            if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && result.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            result,
                            AppUpdateType.IMMEDIATE,
                            StartActivity.this,
                            CHECK_UPDATE_REQUEST_CODE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.Widget_Alert_DialogTheme)
                        .setCancelable(false)
                        .setTitle("Try again!")
                        .setMessage("Update failed\nDo you want to try again?")
                        .setPositiveButton("Yes", (dialog, which) -> {
                            checkForAppUpdate();
                            dialog.dismiss();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> {
                            dialog.dismiss();
                            Snackbar updateCancelBar = Snackbar.make(findViewById(R.id.drawerLayout), "You can still update this app from Playstore.", 3000);
                            updateCancelBar.setAction("Ok", v -> {
                                updateCancelBar.dismiss();
                            });
                            updateCancelBar.show();
                        })
                        .setNeutralButton("Later", (dialog, which) -> {
                            dialog.dismiss();
                            Snackbar updateLaterBar = Snackbar.make(findViewById(R.id.drawerLayout), "As you wish!", 3000);
                            updateLaterBar.setAction(":)", v -> {
                                updateLaterBar.dismiss();
                            });
                            updateLaterBar.show();
                        })
                        .create();
                alertDialog.show();
            } else {
                Snackbar updateSuccessBar = Snackbar.make(findViewById(R.id.drawerLayout), "App updated successfully!", 3000);
                updateSuccessBar.setAction("\uD83D\uDC4D", v -> {
                    updateSuccessBar.dismiss();
                });
                updateSuccessBar.show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1001 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startActivity(new Intent(StartActivity.this, MyVideosActivity.class));
            showAdmobInterstitial();
        } else if (requestCode == 1002 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startActivity(new Intent(StartActivity.this, AllCategoriesActivity.class));
            showAdmobInterstitial();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadAdmobInterStitial();
        loadNativeAdsAdmob();
        refreshDialogAd(adFrame);
    }

    public void loadAdmobInterStitial() {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(
                R.string.adm_interstitial_ad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    private void showAdmobInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    public void onBackPressed() {
        showExitSheet();
    }

    private void showExitSheet() {
        mDialog.findViewById(R.id.btn_exit).setOnClickListener(view -> {
            mDialog.dismiss();
            finish();
            System.exit(0);
        });
        mDialog.setOnDismissListener(dialog -> {
            refreshDialogAd(adFrame);
        });
        mDialog.show();
    }

    public void refreshDialogAd(FrameLayout adFrame) {
        if (mUnifiedNativeAd != null) {
            mUnifiedNativeAd.destroy();
        }
        new AdLoader.Builder(this, getResources().getString(R.string.adm_native_ad)).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) getLayoutInflater().inflate(R.layout.admob_native_home, null);
            mUnifiedNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mUnifiedNativeAd, unifiedNativeAdView);
            adFrame.removeAllViews();
            adFrame.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        new AdLoader.Builder(this, getResources().getString(R.string.adm_native_ad))
                .forUnifiedNativeAd(unifiedNativeAd -> {
                    if (mUnifiedNativeAd != null) {
                        mUnifiedNativeAd.destroy();
                    }
                    mUnifiedNativeAd = unifiedNativeAd;
                    populateUnifiedNativeAdView(mUnifiedNativeAd, adView);
                    frameLayout.setVisibility(View.VISIBLE);
                    frameLayout.removeAllViews();
                    frameLayout.addView(adView);
                }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_rateUs:
                drawerLayout.closeDrawer(GravityCompat.START);
                if (!PreferenceUtility.getInstance(StartActivity.this).getRated()) {
                    isShowing = false;
                    showRatingDialog();
                } else {
                    Toast.makeText(StartActivity.this, "You've rated us already!", Toast.LENGTH_SHORT).show();
                    PreferenceUtility.getInstance(StartActivity.this).setRated(false);
                }
                break;
            case R.id.nav_shareApp:
                drawerLayout.closeDrawer(GravityCompat.START);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                String shareText = getResources().getString(R.string.share_app_desc, BuildConfig.APPLICATION_ID);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                startActivity(Intent.createChooser(shareIntent, "Share app"));
                break;
            case R.id.nav_feedback:
                drawerLayout.closeDrawer(GravityCompat.START);
                onFeedbackGiven();
                break;
            default:
                return;
        }
    }

    @Override
    public void onFeedbackGiven() {
        feedbackSheetBuilder = FeedbackSheetBuilder.with(getSupportFragmentManager())
                .title("Feedback Form")
                .setupLayout(StartActivity.this, R.layout.bottom_sheet_feedback);
        View feedbackView = feedbackSheetBuilder.getLayout();
        initFeedbackheet(feedbackView);
        feedbackSheetBuilder.show();
    }

    private void initFeedbackheet(View feedbackView) {
        EditText editMail = feedbackView.findViewById(R.id.editMail);
        EditText editDesc = feedbackView.findViewById(R.id.editFeedback);
        Button sendEmail = feedbackView.findViewById(R.id.SendEmail);
        ImageView ic_close = feedbackView.findViewById(R.id.ic_close);

        sendEmail.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{PreferenceUtility.RECEIVER_ADDRESS});
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            intent.putExtra(Intent.EXTRA_TEXT,
                    "Email" + " : " + editMail.getText().toString() + "\n"
                            + "Description" + " : " + editDesc.getText().toString());
            try {
                PreferenceUtility.getInstance(this).setRated(true);
                startActivity(Intent.createChooser(intent, "Send Mail"));
            } catch (ActivityNotFoundException ex) {
                ex.printStackTrace();
                Toast.makeText(StartActivity.this, getResources().getString(R.string.no_mail_found), Toast.LENGTH_SHORT).show();
            }
        });
        ic_close.setOnClickListener(v -> feedbackSheetBuilder.dismiss());
    }

    private void showRatingDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.best_rating_dialog, null);
        initDialog(view);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setView(view);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        rate_text.setText(getResources().getString(R.string.rating_desc));

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            smiley_view.setSmiley(rating <= 4.5 ? rating : rating - 1);
            this.rating = rating;
        });

        tv_submit.setOnClickListener(v -> {
            if (rating >= 5) {
                PreferenceUtility.getInstance(StartActivity.this).setRated(true);
                alertDialog.dismiss();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            } else if (rating > 0) {
                alertDialog.dismiss();
                feedbackListenerMain.onFeedbackGiven();
            } else {
                Toast.makeText(StartActivity.this, "Ratings can't be empty!", Toast.LENGTH_SHORT).show();
            }
        });

        tv_maybe_later.setOnClickListener(v -> {
            if (isShowing) {
                PreferenceUtility.getInstance(StartActivity.this).setCount(0);
                alertDialog.dismiss();
                finish();
            } else {
                alertDialog.dismiss();
            }
        });
    }

    private void initDialog(View view) {
        rate_text = view.findViewById(R.id.rate_text);
        smiley_view = view.findViewById(R.id.smiley_view);
        ratingBar = view.findViewById(R.id.ratingBar);
        tv_submit = view.findViewById(R.id.tv_submit);
        tv_maybe_later = view.findViewById(R.id.tv_later);
    }
}
