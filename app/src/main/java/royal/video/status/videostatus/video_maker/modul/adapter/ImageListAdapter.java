package royal.video.status.videostatus.video_maker.modul.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import royal.video.status.videostatus.video_maker.modul.model.ImageJsonData;
import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.modul.activity.VideoMakingActivity;

import java.util.ArrayList;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ImageJsonData> imageList;

    public ImageListAdapter(Context context2, ArrayList<ImageJsonData> arrayList) {
        this.context = context2;
        this.imageList = arrayList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(this.context).inflate(R.layout.row_layout_image_list, viewGroup, false));
    }

    public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {
        ImageView imageView;
        int i2;
        if (this.imageList.get(i).getSelectedPath() == null) {
            Glide.with(this.context).load(this.imageList.get(i).getImagePath()).into(myViewHolder.ivVideoImage);
            imageView = myViewHolder.ivPlus;
            i2 = 0;
        } else {
            Glide.with(this.context).load(this.imageList.get(i).getSelectedPath()).into(myViewHolder.ivVideoImage);
            imageView = myViewHolder.ivPlus;
            i2 = 8;
        }
        imageView.setVisibility(i2);
        myViewHolder.cvParentLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoMakingActivity.videoMakingActivity.getNewImage(myViewHolder.getAdapterPosition());
            }
        });
    }

    public int getItemCount() {
        return this.imageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cvParentLayout;
        ImageView ivPlus;
        ImageView ivVideoImage;

        public MyViewHolder(View view) {
            super(view);
            this.cvParentLayout = (CardView) view.findViewById(R.id.cv_parent_layout);
            this.ivPlus = (ImageView) view.findViewById(R.id.iv_plus);
            this.ivVideoImage = (ImageView) view.findViewById(R.id.iv_video_image);
        }
    }
}
