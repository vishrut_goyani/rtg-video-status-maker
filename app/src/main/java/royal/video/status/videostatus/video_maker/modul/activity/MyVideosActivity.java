package royal.video.status.videostatus.video_maker.modul.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.Global;
import royal.video.status.videostatus.video_maker.modul.adapter.MyVideoListAdapter;
import royal.video.status.videostatus.video_maker.modul.model.MyCreationVideoData;
import royal.video.status.videostatus.video_maker.modul.utils.CompareVideo;

public class MyVideosActivity extends AppCompatActivity implements MyVideoListAdapter.VideoSelectListener {
    public static MyVideosActivity mActivity;
    private TextView btnStartCreating;
    private LinearLayout layoutStartCreating;
    MediaMetadataRetriever metadataRetriever;
    ArrayList<MyCreationVideoData> myVideoList = new ArrayList<>();
    private ProgressBar progressVideoList;
    private RecyclerView rvVideoList;
    public StaggeredGridLayoutManager staggeredGridLayoutManager;
    private SwipeRefreshLayout swipeVideoList;
    public MyVideoListAdapter videoListAdapter;
    Toolbar toolbar;
    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;

    private com.google.android.gms.ads.AdView adm_banner;
    RelativeLayout banner_container;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_my_videos);

        init();
        //loadInterstitialAdAdmob();
        loadBannerAdmob();
        mActivity = this;
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        Bundle bundle2 = new Bundle();
        bundle2.putString("screen_name", "My Videos Screen");
        bundle2.putString("full_text", "User's creations list screen opened");
        swipeVideoList.setEnabled(false);
        btnStartCreating.setOnClickListener(view -> {
            startActivity(new Intent(MyVideosActivity.this, AllCategoriesActivity.class));
            finish();
        });
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        swipeVideoList = findViewById(R.id.swipe_video_list);
        btnStartCreating = findViewById(R.id.btn_start_creating);
        layoutStartCreating = findViewById(R.id.layout_start_creating);
        progressVideoList = findViewById(R.id.progress_video_list);
        rvVideoList = findViewById(R.id.rv_video_list);
        banner_container = findViewById(R.id.banner_container);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(getString(R.string.adm_banner_ad));
    }

    public ArrayList<MyCreationVideoData> getVideoList() {
        myVideoList.clear();
        File file = new File(getFilePath(mActivity));
        if (!file.exists()) {
            file.mkdirs();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            Arrays.sort(listFiles, CompareVideo.compareVideo);
        }
        if (listFiles != null && listFiles.length > 0) {
            for (File file2 : listFiles) {
                if (!(file2 == null || file2.getAbsolutePath() == null || file2.getAbsolutePath().isEmpty())) {
                    try {
                        if (file2.getName().endsWith(".mp4") || file2.getName().endsWith(".3gp")) {
                            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                            metadataRetriever = mediaMetadataRetriever;
                            mediaMetadataRetriever.setDataSource(file2.getAbsolutePath());
                            myVideoList.add(new MyCreationVideoData(file2.getName(), file2.getAbsolutePath(), Integer.parseInt(metadataRetriever.extractMetadata(19)), Integer.valueOf(metadataRetriever.extractMetadata(18)).intValue()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            MediaMetadataRetriever mediaMetadataRetriever2 = metadataRetriever;
            if (mediaMetadataRetriever2 != null) {
                mediaMetadataRetriever2.release();
            }
        }
        return myVideoList;
    }

    public void setData() {
        StaggeredGridLayoutManager staggeredGridLayoutManager2 = new StaggeredGridLayoutManager(2, 1);
        staggeredGridLayoutManager = staggeredGridLayoutManager2;
        rvVideoList.setLayoutManager(staggeredGridLayoutManager2);
        videoListAdapter = new MyVideoListAdapter(myVideoList, mActivity, this);
        rvVideoList.setAdapter(videoListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<MyCreationVideoData> videoList = getVideoList();
        myVideoList = videoList;
        if (videoList.size() > 0) {
            setData();
            progressVideoList.setVisibility(View.GONE);
            return;
        }
        layoutStartCreating.setVisibility(View.VISIBLE);
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    public final String getFilePath(Activity activity) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + Global.app_name);
        if (!file.exists()) {
            file.mkdir();
        }
        return file.getAbsolutePath() + File.separator;
    }

    @Override
    public void onVideoSelectListener(int i, MyCreationVideoData myCreationVideoData) {
        startActivity(new Intent(MyVideosActivity.this, PlayMyVideoActivity.class)
                .putExtra("videoFilePath", myCreationVideoData.getFilePath()));
    }

    public void loadInterstitialAdAdmob() {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(
                R.string.adm_interstitial_ad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        //showInterstitialAdAdmob();
        finish();
    }
}
