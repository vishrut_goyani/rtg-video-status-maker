package royal.video.status.videostatus.video_maker.modul.model;

import org.json.JSONArray;

public class VideoJsonData {
    private String Name;
    private JSONArray PostFix;
    private JSONArray PreFix;
    private String VideoPath;

    public VideoJsonData(String str, String str2, JSONArray jSONArray, JSONArray jSONArray2) {
        this.Name = str;
        this.VideoPath = str2;
        this.PreFix = jSONArray;
        this.PostFix = jSONArray2;
    }

    public JSONArray getPostFix() {
        return this.PostFix;
    }

    public JSONArray getPreFix() {
        return this.PreFix;
    }

    public String getVideoPath() {
        return this.VideoPath;
    }
}
