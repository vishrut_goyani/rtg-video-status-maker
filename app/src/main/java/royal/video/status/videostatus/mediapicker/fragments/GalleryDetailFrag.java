package royal.video.status.videostatus.mediapicker.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.help.ConnectionDetector;
import royal.video.status.videostatus.mediapicker.Gallery;
import royal.video.status.videostatus.mediapicker.adapters.MediaAdapter;
import royal.video.status.videostatus.mediapicker.utils.ClickListener;

import static android.content.Context.MODE_PRIVATE;

public class GalleryDetailFrag extends Fragment {

    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefadmob";

    ConnectionDetector connectionDetector;

    int displayad;
    int whichAdFirst;

    Activity activity;

    public static List<Boolean> selected = new ArrayList();
    public String from;
    private MediaAdapter mAdapter;

    private List<String> mediaList = new ArrayList();
    private View rootView;
    private RecyclerView rvDetail;
    LinearLayout banner_container;
    private TextView tbTitle;
    public String title;

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
        private ClickListener clickListener;
        private GestureDetector gestureDetector;

        public void onRequestDisallowInterceptTouchEvent(boolean z) {
        }

        public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        }

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener2) {
            this.clickListener = clickListener2;
            this.gestureDetector = new GestureDetector(context, new SimpleOnGestureListener() {
                public boolean onSingleTapUp(MotionEvent motionEvent) {
                    return true;
                }

                public void onLongPress(MotionEvent motionEvent) {
                    View findChildViewUnder = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
                    if (findChildViewUnder != null) {
                        ClickListener clickListener = clickListener2;
                        if (clickListener != null) {
                            clickListener.onLongClick(findChildViewUnder, recyclerView.getChildPosition(findChildViewUnder));
                        }
                    }
                }
            });
        }

        public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            View findChildViewUnder = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
            if (!(findChildViewUnder == null || this.clickListener == null || !this.gestureDetector.onTouchEvent(motionEvent))) {
                this.clickListener.onClick(findChildViewUnder, recyclerView.getChildPosition(findChildViewUnder));
            }
            return false;
        }
    }

    public static GalleryDetailFrag getInstance(String str, String str2) {
        GalleryDetailFrag galleryDetailFrag = new GalleryDetailFrag();
        galleryDetailFrag.title = str;
        galleryDetailFrag.from = str2;
        return galleryDetailFrag;
    }

    private void init() {
        tbTitle = rootView.findViewById(R.id.tb_title);
        banner_container = rootView.findViewById(R.id.banner_container);
        tbTitle.setText(title);
        rootView.findViewById(R.id.tb_back).setOnClickListener(view -> getActivity().onBackPressed());
        mediaList.clear();
        selected.clear();
        if (this.from == null) {
            getActivity().onBackPressed();
        }
        if (this.from.equals("Images")) {
            this.mediaList.addAll(ImagesFrag.imagesList);
            selected.addAll(ImagesFrag.selected);
        } else {
            this.mediaList.addAll(VideosFrag.videosList);
            selected.addAll(VideosFrag.selected);
        }
        this.rvDetail = rootView.findViewById(R.id.rv_detail);
        banner_container = rootView.findViewById(R.id.banner_container);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.rootView = layoutInflater.inflate(R.layout.fragment_gallery_detail, viewGroup, false);

        sharedpreferences = getActivity().getSharedPreferences(mypreference, MODE_PRIVATE);
        activity = getActivity();

        connectionDetector = new ConnectionDetector(activity.getApplicationContext());
        boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        displayad = sharedpreferences.getInt("displayad", 3);
        whichAdFirst = sharedpreferences.getInt("whichAdFirst", 2);

        init();
        populateRecyclerView();
        return this.rootView;
    }

    private void populateRecyclerView() {
        if (this.from.equals("Images")) {
            this.mAdapter = new MediaAdapter(getActivity(), this.mediaList, selected, false);
        } else {
            this.mAdapter = new MediaAdapter(getActivity(), this.mediaList, selected, true);
        }
        this.rvDetail.setLayoutManager(new GridLayoutManager(getContext(), 3));
        this.rvDetail.getItemAnimator().setChangeDuration(0);
        this.rvDetail.setAdapter(this.mAdapter);
        this.rvDetail.addOnItemTouchListener(new RecyclerTouchListener(getContext(), this.rvDetail, new ClickListener() {
            public void onLongClick(View view, int i) {
            }

            public void onClick(View view, int i) {
                ((Gallery) GalleryDetailFrag.this.getActivity()).sendResult((String) GalleryDetailFrag.this.mediaList.get(i));
            }
        }));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
