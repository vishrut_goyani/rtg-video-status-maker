package royal.video.status.videostatus.video_maker.modul.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import royal.video.status.videostatus.video_maker.modul.global.Globals;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoList;
import royal.video.status.videostatus.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {
    private static final List<Integer> integerList = Arrays.asList(new Integer[]{-16732441, -589767, -539392, -5563878, -4361542, -1012819, -566180, -40820, -11873182, -11724117, -7202133, -18611});
    private Context context;
    public ArrayList<ModelVideoList> videoList;
    VideoSelectListener videoSelectListener;

    public interface VideoSelectListener {
        void onVideoSelectListener(int i, ModelVideoList modelVideoList);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout change_border;
        CardView cvVideoList;
        RelativeLayout imageRelative;
        ImageView ivBackNewHot;
        ImageView ivNew;
        ConstraintLayout layoutVideoName;
        TextView tvVideoName;
        ImageView videoListThumb;

        public MyViewHolder(View view) {
            super(view);
            this.tvVideoName = (TextView) view.findViewById(R.id.tv_video_name);
            this.cvVideoList = (CardView) view.findViewById(R.id.cv_video_list);
            this.imageRelative = (RelativeLayout) view.findViewById(R.id.imageRelative);
            this.ivBackNewHot = (ImageView) view.findViewById(R.id.ivBackNewHot);
            this.ivNew = (ImageView) view.findViewById(R.id.ivNew);
            this.layoutVideoName = (ConstraintLayout) view.findViewById(R.id.layout_video_name);
            this.videoListThumb = (ImageView) view.findViewById(R.id.video_list_thumb);
            this.change_border = (LinearLayout) view.findViewById(R.id.change_border);
        }
    }

    public VideoListAdapter(ArrayList<ModelVideoList> arrayList, Context context2, VideoSelectListener videoSelectListener2) {
        this.context = context2;
        this.videoList = arrayList;
        this.videoSelectListener = videoSelectListener2;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout_video_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int i) {
        ModelVideoList modelVideoList = this.videoList.get(i);
        myViewHolder.tvVideoName.setText(modelVideoList.getTitle());
        int density = (Resources.getSystem().getDisplayMetrics().widthPixels / 2) - Globals.getDensity(8.0d);
        myViewHolder.videoListThumb.setLayoutParams(new RelativeLayout.LayoutParams(density, (modelVideoList.getHeight() * (density - Globals.getDensity(5.0d))) / modelVideoList.getWidth()));
        int size = i % integerList.size();
        if (modelVideoList.getIsHot()) {
            myViewHolder.ivBackNewHot.setVisibility(View.GONE);
            myViewHolder.ivNew.setVisibility(View.GONE);
            myViewHolder.ivBackNewHot.setColorFilter(integerList.get(size).intValue());
        } else if (modelVideoList.getIsNew()) {
            myViewHolder.ivBackNewHot.setVisibility(View.GONE);
            myViewHolder.ivNew.setVisibility(View.GONE);
            myViewHolder.ivBackNewHot.setColorFilter(integerList.get(size).intValue());
        } else {
            myViewHolder.ivBackNewHot.setVisibility(View.GONE);
            myViewHolder.ivNew.setVisibility(View.GONE);
        }
        Glide.with(this.context).load(modelVideoList.getThumbUrl()).into(myViewHolder.videoListThumb);
        myViewHolder.cvVideoList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (videoSelectListener != null) {
                    videoSelectListener.onVideoSelectListener(myViewHolder.getAdapterPosition(), VideoListAdapter.this.videoList.get(myViewHolder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return Math.min(videoList.size(), 10);
    }
}
