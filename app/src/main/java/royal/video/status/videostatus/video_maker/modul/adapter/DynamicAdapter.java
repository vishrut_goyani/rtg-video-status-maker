package royal.video.status.videostatus.video_maker.modul.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.modul.activity.VideoCategoryDetailsActivity;
import royal.video.status.videostatus.video_maker.modul.model.VideoCategoryData;

public class DynamicAdapter extends RecyclerView.Adapter<DynamicAdapter.ViewHolder> {
    Context context;
    public ArrayList<VideoCategoryData> categoryList;

    public DynamicAdapter(Context context, ArrayList<VideoCategoryData> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txt_cat.setText(categoryList.get(position).getName());
        if (position % 7 == 0) {
            holder.txt_cat.setTextColor(Color.parseColor("#CF0000"));
        } else if (position % 7 == 1) {
            holder.txt_cat.setTextColor(Color.parseColor("#008DCF"));
        } else if (position % 7 == 2) {
            holder.txt_cat.setTextColor(Color.parseColor("#BA7E05"));
        } else if (position % 7 == 3) {
            holder.txt_cat.setTextColor(Color.parseColor("#6E00CF"));
        } else if (position % 7 == 4) {
            holder.txt_cat.setTextColor(Color.parseColor("#83D104"));
        } else if (position % 7 == 5) {
            holder.txt_cat.setTextColor(Color.parseColor("#1855EF"));
        } else if (position % 7 == 6) {
            holder.txt_cat.setTextColor(Color.parseColor("#EF18C0"));
        } else {
            holder.txt_cat.setTextColor(Color.parseColor("#808E01"));
        }

        holder.itemView.findViewById(R.id.catCard).setOnClickListener(v -> {
            context.startActivity(new Intent(context, VideoCategoryDetailsActivity.class)
                    .putExtra("catId", categoryList.get(position).getId())
                    .putExtra("catName", categoryList.get(position).getName()));
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_cat;
        TextView txt_cat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_cat = itemView.findViewById(R.id.img_categories);
            txt_cat = itemView.findViewById(R.id.txt_categories);
        }
    }
}