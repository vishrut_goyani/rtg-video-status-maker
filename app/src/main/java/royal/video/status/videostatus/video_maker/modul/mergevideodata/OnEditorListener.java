package royal.video.status.videostatus.video_maker.modul.mergevideodata;

public interface OnEditorListener {
    void onFailure();

    void onProgress(float f);

    void onSuccess();
}
