package royal.video.status.videostatus.video_maker.modul.api;

import royal.video.status.videostatus.video_maker.modul.model.ModelVideoListData;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoListDataByCategory;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("get-templates")
    Call<ModelVideoListData> getVideoList(@Query("sort_by") String str);

    @POST("get-cat-templates")
    Call<ModelVideoListDataByCategory> getVideoListByCategory(@Query("sort_by") String str, @Query("cat_id") String str2);
}
