package royal.video.status.videostatus.interfaces;

public interface FeedbackListener {
    void onFeedbackGiven();
}