package royal.video.status.videostatus.video_maker.modul.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.Global;
import royal.video.status.videostatus.video_maker.modul.adapter.ImageListAdapter;
import royal.video.status.videostatus.video_maker.modul.mergevideodata.OnEditorListener;
import royal.video.status.videostatus.video_maker.modul.mergevideodata.VideoUitls;
import royal.video.status.videostatus.video_maker.modul.mergevideodata.models.CmdList;
import royal.video.status.videostatus.video_maker.modul.mergevideodata.models.EpEditor;
import royal.video.status.videostatus.video_maker.modul.model.ImageJsonData;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoList;
import royal.video.status.videostatus.video_maker.modul.model.VideoJsonData;
import royal.video.status.videostatus.video_maker.modul.utils.Utils;

public class VideoMakingActivity extends AppCompatActivity {
    public static final int REQUEST_PICK = 9162;
    public String backgroundVideoPath = "";
    Button btnExportVideo;
    String compareStr = "";
    String compareString = "";
    public RoundedHorizontalProgressBar rh_progress_bar;
    String destinationVideoFileName = "";
    public PlayerView exoPlayerVideoDetail;
    Toolbar toolbar;
    public ArrayList<ImageJsonData> imageList = new ArrayList<>();
    ImageListAdapter imageListAdapter;
    public static VideoMakingActivity videoMakingActivity;
    GetImageListFromJSON imageListFromJSONTask;
    public String inputVideoPath;
    boolean isCraftingDone = false;
    int isProgressMatch = 0;
    boolean isRunning = true;
    Animation mAnimation;
    Runnable mRunnable = new MyRunnableTask();
    Handler myHandler = new Handler();
    String outputVideoFilePath = "";
    public ProgressBar progressBarExoplayer;
    String pythonFilePath = "";
    public JSONObject pythonJsonObject;
    int roundedImageHeight;
    int roundedImageWidth;
    private RecyclerView rvImageList;
    int selectedImagePosition = 0;
    SimpleExoPlayer simpleExoPlayer;
    String unZipFileName = "";
    String videoDestinationPath = "";
    public long videoDuration;
    public ArrayList<VideoJsonData> videoList = new ArrayList<>();
    ModelVideoList videoObject;

    AlertDialog.Builder builder;

    private UnifiedNativeAd mNativeAd;
    FrameLayout rl_fb_ad;
    FrameLayout frameLayout;
    View view;
    Dialog finalSaveDialog;

    private class GetImageListFromJSON extends AsyncTask<String, String, String> {
        private GetImageListFromJSON() {
        }

        public String doInBackground(String... strArr) {
            try {
                pythonJsonObject = new JSONObject(getStringFromFile(strArr[0]));
                JSONArray jSONArray = pythonJsonObject.getJSONArray("images");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    ArrayList<ImageJsonData> arrayList = imageList;
                    String string = jSONObject.getString(AppMeasurementSdk.ConditionalUserProperty.NAME);
                    int i2 = jSONObject.getInt("w");
                    int i3 = jSONObject.getInt("h");
                    arrayList.add(new ImageJsonData(string, i2, i3, getFilePath(VideoMakingActivity.this) + unZipFileName + File.separator + jSONObject.getString(AppMeasurementSdk.ConditionalUserProperty.NAME), jSONObject.getJSONArray("prefix"), jSONObject.getJSONArray("postfix")));
                }
                videoDuration = Long.parseLong(pythonJsonObject.getJSONObject(MimeTypes.BASE_TYPE_VIDEO).getString("duration"));
            } catch (Exception unused) {
                unused.printStackTrace();
            }
            return null;
        }

        public void onPostExecute(String str) {
            imageListAdapter.notifyDataSetChanged();
        }

        public void onPreExecute() {
            super.onPreExecute();
        }
    }

    class MyRunnableTask implements Runnable {
        MyRunnableTask() {
        }

        public void run() {
            finalSaveDialog.dismiss();
            isProgressMatch = 0;
            rh_progress_bar.setProgress(0);
            Toast.makeText(VideoMakingActivity.this, "Video created!.", Toast.LENGTH_LONG).show();
            myHandler.removeCallbacks(mRunnable);
            isCraftingDone = true;
            if (isRunning) {
                isCraftingDone = false;
                moveToNext();
            }
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video_making);

        view = LayoutInflater.from(this).inflate(R.layout.finalsave_dialog, null);
        init();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        mAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        if (getIntent().getExtras() != null) {
            videoObject = new Gson().fromJson(getIntent().getStringExtra("video_object"), ModelVideoList.class);
        }
        videoMakingActivity = this;
        exoPlayerVideoDetail.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (isPlaying()) {
                    pausePlayer();
                } else {
                    startPlayer();
                }
                return true;
            }
            return false;
        });

        btnExportVideo.setOnClickListener(view -> {
            Bundle bundle1 = new Bundle();
            bundle1.putString("video_id", String.valueOf(videoObject.getId()));
            bundle1.putString("video_name", videoObject.getTitle());
            saveVideo();
        });

        String substring = videoObject.getZipUrl().substring(videoObject.getZipUrl().lastIndexOf(47) + 1);
        if (substring.indexOf(".") > 0) {
            unZipFileName = substring.substring(0, substring.lastIndexOf("."));
        }
        imageListAdapter = new ImageListAdapter(this, imageList);
        rvImageList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvImageList.setAdapter(imageListAdapter);
        pythonFilePath = getFilePath(this) + unZipFileName + File.separator + "python.json";
        imageListFromJSONTask = (GetImageListFromJSON) new GetImageListFromJSON().execute(new String[]{pythonFilePath});
        outputVideoFilePath = getFilePath(this) + unZipFileName + File.separator + "output.mp4";
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.ic_example);
        if (decodeResource != null) {
            File file = new File(new File(getFilePath(this)), getString(R.string.watermark_image));
            if (file.exists()) {
                file.delete();
            }
            saveImageToGallery(file, decodeResource);
            file.getPath();
        }
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        exoPlayerVideoDetail = findViewById(R.id.exo_player_video_detail);
        progressBarExoplayer = findViewById(R.id.progressBar_exoplayer);
        rh_progress_bar = findViewById(R.id.rh_progress_bar);
        rvImageList = findViewById(R.id.rv_image_list);
        btnExportVideo = findViewById(R.id.btn_export_video);

        rl_fb_ad = view.findViewById(R.id.rl_fb_ad);
        frameLayout = view.findViewById(R.id.adFrame);
        rh_progress_bar = view.findViewById(R.id.rh_progress_bar);
    }

    public boolean isPlaying() {
        return simpleExoPlayer.getPlaybackState() == Player.STATE_READY && simpleExoPlayer.getPlayWhenReady();
    }

    private void initializePlayer() {
        SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance((Context) this, (TrackSelector) new DefaultTrackSelector((TrackSelection.Factory) new AdaptiveTrackSelection.Factory()));
        simpleExoPlayer = newSimpleInstance;
        exoPlayerVideoDetail.setPlayer(newSimpleInstance);
        simpleExoPlayer.prepare(new ExtractorMediaSource(Uri.parse(outputVideoFilePath), new DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyVideoMakerApplication")), new DefaultExtractorsFactory(), (Handler) null, (ExtractorMediaSource.EventListener) null));
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        exoPlayerVideoDetail.hideController();
        simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
            public void onLoadingChanged(boolean z) {
            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            public void onPlayerError(ExoPlaybackException exoPlaybackException) {
            }

            public void onPositionDiscontinuity(int i) {
            }

            public void onRepeatModeChanged(int i) {
            }

            public void onSeekProcessed() {
            }

            public void onShuffleModeEnabledChanged(boolean z) {
            }

            public void onTimelineChanged(Timeline timeline, Object obj, int i) {
            }

            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }

            public void onPlayerStateChanged(boolean z, int i) {
                int i2;
                ProgressBar progressBar;
                if (i == 2) {
                    progressBar = progressBarExoplayer;
                    i2 = 0;
                } else {
                    progressBar = progressBarExoplayer;
                    i2 = 4;
                }
                progressBar.setVisibility(i2);
            }
        });
    }

    public void pausePlayer() {
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.getPlaybackState();
    }

    public void startPlayer() {
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.getPlaybackState();
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
        isRunning = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        startPlayer();
        if (Utils.isNetworkAvailable(this))
            loadNativeAdsAdmob();
        isRunning = true;
        if (isRunning)
            pausePlayer();
        if (isCraftingDone) {
            isCraftingDone = false;
            moveToNext();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        simpleExoPlayer.release();
    }

    @Override
    public void onStop() {
        super.onStop();
        simpleExoPlayer.release();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (videoObject == null && getIntent().getExtras() != null) {
            ModelVideoList modelVideoList = (ModelVideoList) new Gson().fromJson(getIntent().getStringExtra("video_object"), ModelVideoList.class);
            videoObject = modelVideoList;
            if (modelVideoList.getZip().indexOf(".") > 0) {
                unZipFileName = videoObject.getZip().substring(0, videoObject.getZip().lastIndexOf("."));
            }
            outputVideoFilePath = getFilePath(this) + unZipFileName + File.separator + "output.mp4";
        }
        initializePlayer();
    }

    public void getNewImage(int i) {
        selectedImagePosition = i;
        try {
            startActivityForResult(new Intent("android.intent.action.GET_CONTENT").setType("image/*"), REQUEST_PICK);
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(this, R.string.crop__pick_error, Toast.LENGTH_LONG).show();
        }
    }

    public String getFilePath(Context context) {
        String absolutePath = context.getFilesDir().getAbsolutePath();
        File file = new File(absolutePath + File.separator + context.getResources().getString(R.string.oreo_zip_directory) + File.separator + ".Temp_Video");
        if (!file.exists()) {
            file.mkdir();
        }
        return file.getAbsolutePath() + File.separator;
    }

    public final String getDestinationPath(Activity activity) {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + Global.app_name);
        if (!file.exists()) {
            file.mkdir();
        }
        return file.getAbsolutePath() + File.separator;
    }

    public String getStringFromFile(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(str));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    fileInputStream.close();
                    return sb.toString();
                }
            }
        } catch (Exception e) {
            return "";
        }
    }

    public void saveVideo() {
        pausePlayer();
        exoPlayerVideoDetail.hideController();
        isProgressMatch = 0;
        rh_progress_bar.setProgress(0);
        rh_progress_bar.invalidate();
        builder = new AlertDialog.Builder(this, R.style.Widget_Story_DialogTheme)
                .setView(view)
                .setCancelable(false);
        if (finalSaveDialog == null)
            finalSaveDialog = builder.create();
        finalSaveDialog.show();
        makeNewQuery();
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        new AdLoader.Builder(this, getResources().getString(R.string.adm_native_ad)).forUnifiedNativeAd(unifiedNativeAd -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    private void makeNewQuery() {
        destinationVideoFileName = "MyVideo_" + unZipFileName + "_" + System.currentTimeMillis() + ".mp4";
        StringBuilder sb = new StringBuilder();
        sb.append(getDestinationPath(this));
        sb.append(destinationVideoFileName);
        videoDestinationPath = sb.toString();
        new File(videoDestinationPath);
        String str2 = videoDestinationPath;
        CmdList cmdList = new CmdList();
        cmdList.add("ffmpeg");
        try {
            JSONArray jSONArray = pythonJsonObject.getJSONArray("e");
            if (jSONArray.length() != 0) {
                for (int i = 0; i < jSONArray.length(); i++) {
                    cmdList.add(replaceString(jSONArray.getString(i)));
                }
            }
            for (int i2 = 0; i2 < imageList.size(); i2++) {
                if (imageList.get(i2).getPrefix().length() != 0) {
                    for (int i3 = 0; i3 < imageList.get(i2).getPrefix().length(); i3++) {
                        cmdList.add(replaceString(imageList.get(i2).getPrefix().getString(i3)));
                    }
                }
                cmdList.add(imageList.get(i2).getSelectedPath() == null ? imageList.get(i2).getImagePath() : imageList.get(i2).getSelectedPath());
                if (imageList.get(i2).getPostfix().length() != 0) {
                    for (int i4 = 0; i4 < imageList.get(i2).getPostfix().length(); i4++) {
                        cmdList.add(replaceString(imageList.get(i2).getPostfix().getString(i4)));
                    }
                }
            }
            JSONArray jSONArray2 = pythonJsonObject.getJSONArray("static_inputs");
            for (int i5 = 0; i5 < jSONArray2.length(); i5++) {
                JSONObject jSONObject = jSONArray2.getJSONObject(i5);
                ArrayList<VideoJsonData> arrayList = videoList;
                String string = jSONObject.getString(AppMeasurementSdk.ConditionalUserProperty.NAME);
                arrayList.add(new VideoJsonData(string, getFilePath(this) + unZipFileName + File.separator + jSONObject.getString(AppMeasurementSdk.ConditionalUserProperty.NAME), jSONObject.getJSONArray("prefix"), jSONObject.getJSONArray("postfix")));
                backgroundVideoPath = videoList.get(0).getVideoPath();
                if (videoList.get(i5).getPreFix().length() != 0) {
                    for (int i6 = 0; i6 < videoList.get(i5).getPreFix().length(); i6++) {
                        cmdList.add(replaceString(videoList.get(i5).getPreFix().getString(i6)));
                    }
                }
                inputVideoPath = videoList.get(i5).getVideoPath();
                cmdList.add(videoList.get(i5).getVideoPath());
                if (videoList.get(i5).getPostFix().length() != 0) {
                    for (int i7 = 0; i7 < videoList.get(i5).getPostFix().length(); i7++) {
                        cmdList.add(replaceString(videoList.get(i5).getPostFix().getString(i7)));
                    }
                }
            }
            cmdList.add("-i");
            cmdList.add(getFilePath(this) + getString(R.string.watermark_image));
            if (!compareString.equals("")) {
                if (!compareStr.equals("")) {
                    cmdList.add("-ss");
                    cmdList.add(compareStr);
                }
                cmdList.add("-i");
                cmdList.add(compareString);
            }
            JSONArray jSONArray3 = pythonJsonObject.getJSONArray("m");
            if (jSONArray3.length() != 0) {
                for (int i8 = 0; i8 < jSONArray3.length(); i8++) {
                    cmdList.add(replaceString(jSONArray3.getString(i8)));
                }
            }
            if (compareString.equals("")) {
                JSONArray jSONArray4 = pythonJsonObject.getJSONArray("r");
                if (jSONArray4.length() != 0) {
                    for (int i9 = 0; i9 < jSONArray4.length(); i9++) {
                        cmdList.add(replaceString(jSONArray4.getString(i9)));
                    }
                }
            } else {
                JSONArray jSONArray5 = pythonJsonObject.getJSONArray("i");
                if (jSONArray5.length() != 0) {
                    for (int i10 = 0; i10 < jSONArray5.length(); i10++) {
                        cmdList.add(replaceString(jSONArray5.getString(i10)));
                    }
                }
            }
            JSONArray jSONArray6 = pythonJsonObject.getJSONArray("n");
            if (jSONArray6.length() != 0) {
                for (int i11 = 0; i11 < jSONArray6.length(); i11++) {
                    cmdList.add(replaceString(jSONArray6.getString(i11)));
                }
            }
            JSONArray jSONArray7 = pythonJsonObject.getJSONArray("g");
            if (jSONArray7.length() != 0) {
                for (int i12 = 0; i12 < jSONArray7.length(); i12++) {
                    cmdList.add(replaceString(jSONArray7.getString(i12)));
                }
            }
            JSONArray jSONArray8 = pythonJsonObject.getJSONArray("c");
            if (jSONArray8.length() != 0) {
                for (int i13 = 0; i13 < jSONArray8.length(); i13++) {
                    cmdList.add(replaceString(jSONArray8.getString(i13)));
                }
            }
            if (!compareString.equals("")) {
                JSONArray jSONArray9 = pythonJsonObject.getJSONArray("o");
                if (jSONArray9.length() != 0) {
                    for (int i14 = 0; i14 < jSONArray9.length(); i14++) {
                        cmdList.add(replaceString(jSONArray9.getString(i14)));
                    }
                }
            }
            JSONArray jSONArray10 = pythonJsonObject.getJSONArray("d");
            if (jSONArray10.length() != 0) {
                for (int i15 = 0; i15 < jSONArray10.length(); i15++) {
                    cmdList.add(replaceString(jSONArray10.getString(i15)));
                }
            }
            JSONArray jSONArray11 = pythonJsonObject.getJSONArray("s");
            if (jSONArray11.length() != 0) {
                for (int i16 = 0; i16 < jSONArray11.length(); i16++) {
                    cmdList.add(replaceString(jSONArray11.getString(i16)));
                }
            }
            if (compareString.equals("")) {
                cmdList.add("-c:a");
                cmdList.add("copy");
            }
            cmdList.add("-flags");
            cmdList.add("+global_header");
            cmdList.add(str2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            fire_Command(cmdList);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final String replaceString(String str) {
        return str.replace("{pythoncomplex}", "filter_complex").replace("{pythonmerge}", "alphamerge").replace("{pythono}", "overlay").replace("{pythonz}", "zoom").replace("{pythonf}", "fade");
    }


    public void fire_Command(CmdList cmdList) {
        long duration = VideoUitls.getDuration(inputVideoPath);
        EpEditor.execCmd(cmdList, duration, new OnEditorListener() {
            public void onFailure() {
                new Handler(Looper.getMainLooper()) {
                    public void handleMessage(Message message) {
                        Toast.makeText(VideoMakingActivity.this, "fail", Toast.LENGTH_LONG).show();
                    }
                };
            }

            @Override
            public void onProgress(float f) {
                float f2 = f * 100.0f;
                rh_progress_bar.setProgress((int) f2);

            }

            @Override
            public void onSuccess() {
                MediaScannerConnection.scanFile(getApplicationContext(), new String[]{new File(videoDestinationPath).getAbsolutePath()}, new String[]{"mp4"}, null);
                myHandler.postDelayed(mRunnable, 500);
            }
        });
    }

    @Override
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 9162 && i2 == -1) {
            beginCrop(intent.getData());
        } else if (i == 203 && i2 == -1) {
            imageList.get(selectedImagePosition).setSelectedPath(CropImage.getActivityResult(intent).getUri().toString());
            imageListAdapter.notifyDataSetChanged();
        } else if (i2 == 204) {
            CropImage.getActivityResult(intent).getError();
        }
    }

    private void beginCrop(Uri uri) {
        roundedImageWidth = Math.round((float) imageList.get(selectedImagePosition).getWidth());
        int round = Math.round((float) imageList.get(selectedImagePosition).getHeight());
        roundedImageHeight = round;
        int gcdThing = gcdThing(roundedImageWidth, round);
        Uri.fromFile(new File(getCacheDir(), "cropped"));
        CropImage.activity(uri).setAspectRatio(roundedImageWidth / gcdThing, roundedImageHeight / gcdThing).start(this);
    }

    private static int gcdThing(int i, int i2) {
        return BigInteger.valueOf(i).gcd(BigInteger.valueOf(i2)).intValue();
    }

    public void moveToNext() {
        Intent intent = new Intent(this, PreviewBeforeExportActivity.class);
        intent.putExtra("filePath", videoDestinationPath);
        intent.putExtra("fileName", destinationVideoFileName);
        intent.putExtra("video_object", new Gson().toJson(videoObject));
        startActivity(intent);
    }

    public static File saveImageToGallery(File file, Bitmap bitmap) {
        if (bitmap != null) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return file;
        }
        throw new IllegalArgumentException("bmp should not be null");
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
