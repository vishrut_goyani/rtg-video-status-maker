package royal.video.status.videostatus.video_maker.modul.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.video_maker.modul.fragment.DynamicFragment;

public class VideoCategoryDetailsActivity extends AppCompatActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_category_details);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((TextView) toolbar.findViewById(R.id.app_title)).setText(getIntent().getStringExtra("catName"));
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.catFrame, DynamicFragment.newInstance(getIntent().getIntExtra("catId", -1), getIntent().getStringExtra("catName")))
                .commit();
    }
}