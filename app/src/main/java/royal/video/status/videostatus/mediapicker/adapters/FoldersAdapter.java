package royal.video.status.videostatus.mediapicker.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

import royal.video.status.videostatus.mediapicker.fragments.ImagesFrag;
import royal.video.status.videostatus.mediapicker.utils.ScreenUtil;
import royal.video.status.videostatus.R;

import java.util.List;

public class FoldersAdapter extends RecyclerView.Adapter<FoldersAdapter.MyViewHolder> {
    private Activity activity;
    private List<String> bitmapList;
    private List<String> folderNames;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView count;
        public ImageView thumbnail;
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.tv_title);
            this.count = (TextView) view.findViewById(R.id.tv_count);
            this.thumbnail = (ImageView) view.findViewById(R.id.iv_thumb);
        }
    }

    public FoldersAdapter(Activity activity2, List<String> list, List<String> list2) {
        this.folderNames = list;
        this.bitmapList = list2;
        this.activity = activity2;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_folder, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        myViewHolder.thumbnail.getLayoutParams().width = ScreenUtil.getScreenWidth(this.activity) / 3;
        myViewHolder.thumbnail.getLayoutParams().height = ScreenUtil.getScreenWidth(this.activity) / 3;
        if (!TextUtils.isEmpty(folderNames.get(position)))
            myViewHolder.title.setText(folderNames.get(position));
        else ImagesFrag.rvImages.getChildLayoutPosition(myViewHolder.itemView);
        RequestManager with = Glide.with(activity);
        StringBuilder sb = new StringBuilder();
        sb.append("file://");
        sb.append((String) this.bitmapList.get(position));
        with.load(sb.toString()).apply(((RequestOptions) new RequestOptions().override(300, 300)).centerCrop()).into(myViewHolder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return this.folderNames.size();
    }
}
