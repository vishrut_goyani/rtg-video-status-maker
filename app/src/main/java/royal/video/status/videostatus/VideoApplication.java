package royal.video.status.videostatus;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class VideoApplication extends Application {

    private static VideoApplication instance;
//    AppOpenManager appOpenManager;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized VideoApplication getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseAnalytics.getInstance(this);
        FirebaseCrashlytics.getInstance();
//        MobileAds.initialize(this);
//        appOpenManager = new AppOpenManager(this);
    }
}
