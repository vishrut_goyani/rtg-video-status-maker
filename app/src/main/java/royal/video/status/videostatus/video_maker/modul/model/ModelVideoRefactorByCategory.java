package royal.video.status.videostatus.video_maker.modul.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModelVideoRefactorByCategory {
    @SerializedName("templates")
    private ArrayList<ModelVideoList> TemplatesList;

    public ArrayList<ModelVideoList> getTemplatesList() {
        return this.TemplatesList;
    }
}
