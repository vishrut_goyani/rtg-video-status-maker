package royal.video.status.videostatus.widgets;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import royal.video.status.videostatus.R;

public class FeedbackSheetBuilder extends BottomSheetDialogFragment {

    FragmentManager fragmentManager;
    String title;
    View layout;

    public static FeedbackSheetBuilder with(FragmentManager manager) {
        FeedbackSheetBuilder fragment = new FeedbackSheetBuilder();
        fragment.fragmentManager = manager;
        return fragment;
    }

    public FeedbackSheetBuilder title(String title) {
        this.title = title;
        return this;
    }

    public FeedbackSheetBuilder setupLayout(Context context, int layoutResId) {
        layout = LayoutInflater.from(context).inflate(layoutResId, null);
        return this;
    }

    public View getLayout() {
        if (layout != null)
            return layout;
        return null;
    }

    public void show() {
        show(fragmentManager, getTag());
    }

    @Override
    public int getTheme() {
        return R.style.BottomSheetDialogTheme;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = null;
        if (layout != null) {
            contentView = layout;
        }

        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

}