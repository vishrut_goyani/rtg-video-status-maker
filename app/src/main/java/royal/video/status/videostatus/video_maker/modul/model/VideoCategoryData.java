package royal.video.status.videostatus.video_maker.modul.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VideoCategoryData implements Serializable {
    @SerializedName("id")
    private int vidId;
    @SerializedName("image_url")
    private String ImageUrl;
    @SerializedName("name")
    private String Name;
    private int drawableCount;
    private String drawablecounts;

    public VideoCategoryData(int vidId, String name, int drawableCount, String drawablecounts) {
        this.vidId = vidId;
        this.Name = name;
        this.drawableCount = drawableCount;
        this.drawablecounts = drawablecounts;
    }

    public String getDrawableCounts() {
        return this.drawablecounts;
    }

    public int getId() {
        return this.vidId;
    }

    public String getImageUrl() {
        return this.ImageUrl;
    }

    public String getName() {
        return this.Name;
    }

    public int getDrawableCount() {
        return this.drawableCount;
    }

    public void setDrawableCount(int i) {
        this.drawableCount = i;
    }
}
