package royal.video.status.videostatus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.fragment.app.DialogFragment;

import royal.video.status.videostatus.R;

import dev.yuganshtyagi.smileyrating.SmileyRatingView;

public class AppRatingDialog extends DialogFragment {

    SmileyRatingView smiley_view;
    AppCompatRatingBar ratingBar;
    static Activity activity;
    TextView tv_submit, tv_maybe_later, rate_text;
    float rating;

    public AppRatingDialog() {
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    public static AppRatingDialog show(@NonNull AppCompatActivity appCompatActivity) {
        AppRatingDialog appRatingDialog = new AppRatingDialog();
        AppRatingDialog.activity = appCompatActivity;
        appRatingDialog.show(appCompatActivity.getSupportFragmentManager(), "AppRatingDialog");
        return appRatingDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setFlags(1024, 1024);
        View view = inflater.inflate(R.layout.rating_dialog, container, false);
        init(view);

        rate_text.setText(activity.getResources().getString(R.string.rating_desc, activity.getResources().getString(R.string.app_name)));

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            smiley_view.setSmiley(rating <= 4.5 ? rating : rating - 1);
            this.rating = rating;
        });

        tv_submit.setOnClickListener(v -> {

            if (rating > 0) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="
                        + activity.getPackageName())));
                dismiss();
            } else {
                ratingBar.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.shake));
            }
        });

        tv_maybe_later.setOnClickListener(v -> {
            dismiss();
        });

        return view;
    }

    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(-16777216));
        }
    }

    private void init(View view) {
        rate_text = view.findViewById(R.id.rate_text);
        smiley_view = view.findViewById(R.id.smiley_view);
        ratingBar = view.findViewById(R.id.ratingBar);
        tv_submit = view.findViewById(R.id.tv_submit);
        tv_maybe_later = view.findViewById(R.id.tv_later);
    }


}