package royal.video.status.videostatus.video_maker.modul.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import royal.video.status.videostatus.R;
import royal.video.status.videostatus.help.ConnectionDetector;
import royal.video.status.videostatus.video_maker.modul.api.APIClient;
import royal.video.status.videostatus.video_maker.modul.api.APIInterface;
import royal.video.status.videostatus.video_maker.modul.model.ModelVideoList;
import royal.video.status.videostatus.video_maker.modul.utils.Utils;

public class PlayVideoActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST = 5555;
    public static String picturePath;
    public static PlayVideoActivity playVideoActivity;
    public static String unZipFilePath;
    File TempDownloadFile = null;
    APIInterface apiInterface;
    Button btnCreateVideo;
    TextView btnTryAgain;
    Cache cache;
    int downloadCount;
    File downloadFile = null;
    public String downloadFileName = "";
    public String downloadUrl = "";
    public PlayerView exoPlayerVideoDetail;
    public boolean isClosed;
    boolean isCreateButtonClick = false;
    boolean isDownloading = false;
    boolean isRunning = true;
    boolean isUnZipDone = false;
    LinearLayout layoutTryAgain;
    Dialog mDialog;
    TextView percentageTextView;
    public ProgressBar progressBarExoplayer;
    int progressCount = 0;
    RoundedHorizontalProgressBar roundedHorizontalProgressBar;
    SimpleExoPlayer simpleExoPlayer;
    ModelVideoList videoObject;
    Toolbar toolbar;

    LinearLayout nativeAdView;
    private UnifiedNativeAd mNativeAd;
    FrameLayout frameLayout;
    View view;

    private com.google.android.gms.ads.AdView adm_banner;
    RelativeLayout banner_container;

    public void callDownloadVideoApi() {
    }

    public static class VideoCache {
        private static SimpleCache sDownloadCache;

        public static SimpleCache getInstance(Context context) {
            if (sDownloadCache == null) {
                sDownloadCache = new SimpleCache(new File(context.getCacheDir(), "exoCache"), new LeastRecentlyUsedCacheEvictor(1073741824));
            }
            return sDownloadCache;
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_play_video);
        getWindow().setFlags(1024, 1024);
        playVideoActivity = this;

        view = LayoutInflater.from(this).inflate(R.layout.video_download_dialog, null);
        frameLayout = view.findViewById(R.id.adFrame);
        init();
        loadBannerAdmob();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getIntent().getExtras() != null) {
            videoObject = new Gson().fromJson(getIntent().getStringExtra("video_object"), ModelVideoList.class);
        }
        if (videoObject != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("video_id", String.valueOf(videoObject.getId()));
            bundle2.putString("video_name", videoObject.getTitle());
        }
        isPermissionGiven();
        ConnectionDetector connectionDetector = new ConnectionDetector(this);
        boolean isInternetPresent = connectionDetector.isConnectingToInternet();
        if (isInternetPresent)
            setDownloadDialog();
        else Toast.makeText(playVideoActivity, "No Internet!", Toast.LENGTH_SHORT).show();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        btnCreateVideo.setOnClickListener(view -> {
            playVideoActivity.isCreateButtonClick = true;
            if (playVideoActivity.isPermissionGiven()) {
                btnCreateVideo.setEnabled(false);
                Bundle bundle1 = new Bundle();
                bundle1.putString("video_id", String.valueOf(videoObject.getId()));
                bundle1.putString("video_name", videoObject.getTitle());
                createVideo();
            }
        });
        btnTryAgain = findViewById(R.id.btn_try_again);
        progressBarExoplayer = findViewById(R.id.progressBar_exoplayer);
        btnTryAgain.setOnClickListener(view -> {
            layoutTryAgain.setVisibility(View.GONE);
            progressBarExoplayer.setVisibility(View.GONE);
            initializePlayer();
        });
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        btnCreateVideo = findViewById(R.id.btn_create_video);
        layoutTryAgain = findViewById(R.id.layout_try_again);
        exoPlayerVideoDetail = findViewById(R.id.exo_player_video_detail);

        banner_container = findViewById(R.id.banner_container);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(getString(R.string.adm_banner_ad));
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    @Override
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == MY_PERMISSIONS_REQUEST && iArr != null && iArr.length > 0) {
            if (iArr[0] == -1) {
                Toast.makeText(this, "Permission denied, To be continue you have to allow the permission.", Toast.LENGTH_LONG).show();
            } else if (iArr[0] == 0 && isCreateButtonClick) {
                isCreateButtonClick = false;
                Bundle bundle = new Bundle();
                bundle.putString("video_id", String.valueOf(videoObject.getId()));
                bundle.putString("video_name", videoObject.getTitle());
                createVideo();
            }
        }
    }

    public boolean isPermissionGiven() {
        if (Build.VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == 0 /*|| ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == 0*/) {
            return true;
        }
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST);
        return false;
    }

    public boolean checkForDownloaded() {
        String zipUrl = videoObject.getZipUrl();
        downloadUrl = zipUrl;
        String[] split = zipUrl.split("/");
        downloadFileName = split[split.length - 1];
        File filesDir = getFilesDir();
        File file = new File(filesDir.getAbsolutePath() + File.separator + getResources().getString(R.string.oreo_zip_directory));
        TempDownloadFile = file;
        if (!file.exists()) {
            return false;
        }
        File file2 = new File(TempDownloadFile, ".Temp_Video");
        TempDownloadFile = file2;
        if (!file2.exists()) {
            return false;
        }
        downloadFile = new File(TempDownloadFile, downloadFileName);
        File file3 = new File(TempDownloadFile, downloadFileName.split(".zip")[0]);
        if (!file3.exists() || file3.length() <= 0) {
            return false;
        }
        picturePath = file3.getPath();
        return true;
    }

    public void initializePlayer() {
        ExtractorMediaSource extractorMediaSource;
        SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance((Context) this, (TrackSelector) new DefaultTrackSelector((TrackSelection.Factory) new AdaptiveTrackSelection.Factory()));
        simpleExoPlayer = newSimpleInstance;
        exoPlayerVideoDetail.setPlayer(newSimpleInstance);
        if (checkForDownloaded()) {
            extractorMediaSource = new ExtractorMediaSource(Uri.parse(picturePath + File.separator + "output.mp4"), new DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyVideoMakerApplication")), new DefaultExtractorsFactory(), (Handler) null, (ExtractorMediaSource.EventListener) null);
        } else {
            extractorMediaSource = new ExtractorMediaSource.Factory(new CacheDataSourceFactory(VideoCache.getInstance(this), new DefaultDataSourceFactory(this, "MyVideoMakerApplication"))).createMediaSource(Uri.parse(videoObject.getVideoUrl()));
            cache = null;
        }
        simpleExoPlayer.prepare(extractorMediaSource);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        exoPlayerVideoDetail.hideController();
        simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
            public void onLoadingChanged(boolean z) {
            }

            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            }

            public void onPositionDiscontinuity(int i) {
            }

            public void onRepeatModeChanged(int i) {
            }

            public void onSeekProcessed() {
            }

            public void onShuffleModeEnabledChanged(boolean z) {
            }

            public void onTimelineChanged(Timeline timeline, Object obj, int i) {
            }

            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }

            public void onPlayerError(ExoPlaybackException exoPlaybackException) {
                if (exoPlaybackException != null && exoPlaybackException.getMessage() != null && exoPlaybackException.getMessage().contains("Unable to connect")) {
                    exoPlayerVideoDetail.hideController();
                    layoutTryAgain.setVisibility(View.VISIBLE);
                }
            }

            public void onPlayerStateChanged(boolean z, int i) {
                int i2;
                ProgressBar progressBar;
                if (i == 2) {
                    progressBar = progressBarExoplayer;
                    i2 = 0;
                } else {
                    progressBar = progressBarExoplayer;
                    i2 = 4;
                }
                progressBar.setVisibility(i2);
            }
        });
    }

    public void pausePlayer() {
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.getPlaybackState();
    }

    private void startPlayer() {
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.getPlaybackState();
    }

    public void InitDialog() {
        Dialog dialog = mDialog;
        if (dialog != null && dialog.isShowing()) {
            deleteFile();
            mDialog.dismiss();
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    btnCreateVideo.setEnabled(true);
                }
            }, 500);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPause() {
        super.onPause();
        isClosed = true;
        pausePlayer();
        isRunning = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isClosed = false;
        isRunning = true;
        Dialog dialog = mDialog;
        if (frameLayout != null)
            loadNativeAdsAdmob();

        if (dialog != null && !dialog.isShowing()) {
            startPlayer();
        } else if (isUnZipDone) {
            btnCreateVideo.setEnabled(true);
            picturePath = downloadFile.getPath();
            if (isDownloading) {
                Dialog dialog2 = mDialog;
                if (dialog2 != null) {
                    dialog2.dismiss();
                }
                isDownloading = false;
                isUnZipDone = false;
            }
            MoveToNext();
        }
    }

    public void onStart() {
        super.onStart();
        if (videoObject == null && getIntent().getExtras() != null) {
            videoObject = (ModelVideoList) new Gson().fromJson(getIntent().getStringExtra("video_object"), ModelVideoList.class);
        }
        initializePlayer();
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        simpleExoPlayer.release();
    }

    public void onStop() {
        super.onStop();
        simpleExoPlayer.release();
    }

    public void deleteFile() {
        if (downloadFile.exists()) {
            if (downloadFile.delete()) {
                callBroadCast();
            } else {
            }
        }
        if (!downloadFile.exists()) {
            return;
        }
        if (downloadFile.delete()) {
            callBroadCast();
            return;
        }
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            MediaScannerConnection.scanFile(this, new String[]{getFilesDir().getAbsolutePath()}, (String[]) null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String str, Uri uri) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("-> uri=");
                    sb.append(uri);
                }
            });
            return;
        }
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
    }

    public void setDownloadDialog() {
        if (videoObject != null) {
            Dialog dialog = new Dialog(this, R.style.Widget_Story_DialogTheme);
            mDialog = dialog;
            mDialog.requestWindowFeature(1);
            mDialog.setContentView(view);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.setCancelable(false);

            ((TextView) mDialog.findViewById(R.id.tv_effect_name)).setText(videoObject.getTitle());
            roundedHorizontalProgressBar = mDialog.findViewById(R.id.rh_progress_bar);
            percentageTextView = mDialog.findViewById(R.id.per_txt);
            mDialog.findViewById(R.id.bt_cancel).setOnClickListener(view -> {
                playVideoActivity.isDownloading = false;
                PRDownloader.cancel(playVideoActivity.downloadCount);
                percentageTextView.setText("Downloading... ");
                roundedHorizontalProgressBar.animateProgress(0, 0);
                roundedHorizontalProgressBar.animateProgress(0, progressCount, 0);
                roundedHorizontalProgressBar.setProgress(0);
                InitDialog();
            });
        }
    }

    public void downloadFile() {
        try {
            mDialog.show();
            isDownloading = true;
            downloadCount = PRDownloader.download(downloadUrl, TempDownloadFile.getPath(), downloadFileName).build().setOnStartOrResumeListener(new OnStartOrResumeListener() {
                public void onStartOrResume() {
                }
            }).setOnPauseListener(() -> {
            }).setOnCancelListener(() -> {
            }).setOnProgressListener(progress -> {
                long j = (progress.currentBytes * 100) / progress.totalBytes;
                TextView textView = percentageTextView;
                StringBuilder sb = new StringBuilder();
                sb.append("Downloading... ");
                int i = (int) j;
                sb.append(i);
                sb.append("%");
                textView.setText(sb.toString());
                roundedHorizontalProgressBar.animateProgress(0, progressCount, i);
                progressCount = i;
            }).start(new OnDownloadListener() {
                public void onDownloadComplete() {
                    try {
                        if (downloadFile != null) {
                            unzip(downloadFile.getPath(), TempDownloadFile.getAbsolutePath());
                            return;
                        }
                        new Handler().postDelayed(() -> btnCreateVideo.setEnabled(true), 3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                        new Handler().postDelayed(() -> btnCreateVideo.setEnabled(true), 3000);
                    }
                }

                public void onError(Error error) {
                    Toast.makeText(PlayVideoActivity.this, error.getServerErrorMessage(), Toast.LENGTH_LONG).show();
                    percentageTextView.setText("Downloading... ");
                    roundedHorizontalProgressBar.animateProgress(0, 0);
                    roundedHorizontalProgressBar.animateProgress(0, progressCount, 0);
                    roundedHorizontalProgressBar.setProgress(0);
                    InitDialog();
                    Dialog dialog = mDialog;
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
        } catch (Exception e) {
            percentageTextView.setText("Downloading... ");
            roundedHorizontalProgressBar.animateProgress(0, 0);
            roundedHorizontalProgressBar.animateProgress(0, progressCount, 0);
            roundedHorizontalProgressBar.setProgress(0);
            InitDialog();
            Dialog dialog = mDialog;
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    public void createVideo() {
        callDownloadVideoApi();
        if (Utils.isNetworkAvailable(this)) {
            String zipUrl = videoObject.getZipUrl();
            downloadUrl = zipUrl;
            String[] split = zipUrl.split("/");
            downloadFileName = split[split.length - 1];
            File filesDir = getFilesDir();
            File file = new File(filesDir.getAbsolutePath() + File.separator + getResources().getString(R.string.oreo_zip_directory));
            TempDownloadFile = file;
            if (!file.exists()) {
                TempDownloadFile.mkdir();
            }
            File file2 = new File(TempDownloadFile, ".Temp_Video");
            TempDownloadFile = file2;
            if (!file2.exists()) {
                TempDownloadFile.mkdir();
            }
            downloadFile = new File(TempDownloadFile, downloadFileName);
            File file3 = new File(TempDownloadFile, downloadFileName.split(".zip")[0]);
            if (file3.exists()) {
                unZipFilePath = file3.getPath();
                MoveToNext();
                return;
            }
            pausePlayer();
            downloadFile();
            return;
        }
        Toast.makeText(this, "Please Connect to Internet.", Toast.LENGTH_LONG).show();
    }

    public final void MoveToNext() {
        btnCreateVideo.setEnabled(true);
        Intent intent = new Intent(this, VideoMakingActivity.class);
        intent.putExtra("fromBackground", false);
        intent.putExtra("video_object", new Gson().toJson((Object) videoObject));
        startActivity(intent);
    }

    public void unzip(String str, String str2) {
        try {
            ZipFile zipFile = new ZipFile(new File(str));
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                unzipEntry(zipFile, (ZipEntry) entries.nextElement(), str2);
            }
            isUnZipDone = true;
            if (isRunning) {
                btnCreateVideo.setEnabled(true);
                picturePath = downloadFile.getPath();
                if (isDownloading) {
                    Dialog dialog = mDialog;
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    isDownloading = false;
                    isUnZipDone = false;
                }
                MoveToNext();
            }
        } catch (Exception e) {
        }
    }

    private void unzipEntry(ZipFile zipFile, ZipEntry zipEntry, String str) throws IOException {
        if (zipEntry.isDirectory()) {
            createDir(new File(str, zipEntry.getName()));
            return;
        }
        File file = new File(str, zipEntry.getName());
        if (!file.getParentFile().exists()) {
            createDir(file.getParentFile());
        }
        BufferedInputStream bufferedInputStream = new BufferedInputStream(zipFile.getInputStream(zipEntry));
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
        while (true) {
            int read = bufferedInputStream.read();
            if (read != -1) {
                bufferedOutputStream.write(read);
            } else {
                bufferedOutputStream.close();
                return;
            }
        }
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        new AdLoader.Builder(this, getResources().getString(R.string.adm_native_ad)).forUnifiedNativeAd(unifiedNativeAd -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    private void createDir(File file) {
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new RuntimeException("Can not create dir " + file);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
